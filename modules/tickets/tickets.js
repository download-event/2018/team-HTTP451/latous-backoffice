/**
 * Created by paolo on 10/09/2017.
 */
$(function () {
    $(document).ready(function () {
        $.module.loadMenu($userStore.getState().role);
        /**************************************/
        /* list  form di base*/
        /*************************************/
        var params = {"fields":["id","clientCode","companyId","group","category","status"]}
        $.when(   $.module.listTicket($rootingStore.getState().modulo, "none",$userStore.getState().role, $userStore.getState().department) )
           .then(
            function( data ) {
                console.log("data", data);
                // for (i = 0; i < data.length; i++) {
                //     data[i].plusOne = data[i].issueIds.length();
                // }

                //caso particolare delle liste le gestisco tutto qui. manipolazioni di dati
                var myTmpl =$.templates[$rootingStore.getState().modulo+"List"];
                $( "#listHolder" ).append(
                    myTmpl.render({"items":data} )
                );
                loadTable();



            }
        );
        /**************************************/
        /* load table
        /*************************************/
        function loadTable() {
            $('#GenericTable').DataTable( {
                responsive: true
            } );

        }
        /**************************************/
        /* update   di base
        /*************************************/
        $.fn.save = function () {
            $.when(   $.module.savedata() )
                .then(
                    function( data ) {
                        console.log("savedata in clientjs", data);
                        $.module.update($rootingStore.getState().modulo, $rootingStore.getState().id, data);
                    }
                );
        }
        /**************************************/
        /* create   di base
        /*************************************/
        $.fn.create = function () {
            $.when(   $.module.savedata() )
                .then(
                    function( data ) {
                        console.log("createed in clientjs", data);
                        $.module.create($rootingStore.getState().modulo,  data);
                    }
                );
        }
        /**************************************/
        /* add issues to ticket   di base
        /*************************************/
        $.fn.addIssues = function () {
            $.when(   loadissue() )
                .then(
                    function( data ) {
                        console.log("issues", data);
                        $('#generic-modal').modal('show');
                        $("#InModal").empty();
                        $(".modal-title").text("Assegna Le Segnalazioni al ticket");
                        // $("#InModal").append(
                        //     $("#" + template + "tutto").render(prova)
                        // );
                        var myTmpl =$.templates[$rootingStore.getState().modulo+"Modal"];
                        $( "#InModal" ).append(
                            myTmpl.render({"items":data} )
                        );
                    }
                );
        };
        $.fn.addIssuesRow = function (id, category, description) {
            // console.log("issuesAdd",issuesAdd);
            var itemIss = {
                _id:id,
                category :category,
                description:description
            }
            var myTmpl =$.templates[$rootingStore.getState().modulo+"ListIssues"];
            $( "#listIssues" ).append(
                myTmpl.render(itemIss)
            );
        }



        function loadissue() {
            var dfd = jQuery.Deferred();
            var req2 = $.DataAccess.listDuplicate("issues", "none", localStorage.issueForTicketid);
            req2.success(function (json) {
                var utente = json;

                dfd.resolve(utente);
                // compileForm(data);
            });
            return dfd;

        }

    }); //document ready




});




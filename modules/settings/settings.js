/**
 * Created by paolo on 10/09/2017.
 */
$(function () {

    $(document).ready(function () {
        $.module.loadMenu(localStorage.role);
        /**************************************/
        /* list  form di base*/
        /*************************************/
        var params = {"fields":["id","clientCode","companyId","group","category","status"]}
        $.when(   $.module.list(localStorage.modulo, params) )
           .then(
            function( data ) {
                console.log("data", data);
                //caso particolare delle liste le gestisco tutto qui. manipolazioni di dati
                $( "#listHolder" ).append(
                    $( "#"+localStorage.modulo+"List" ).render( {"items":data} )
                );
                loadTable();
            }
        );


        /*
          List file for servizi
          -----------------------------------------------------------*/
        function loadUsers() {
            var dfd = jQuery.Deferred();
            var req2 = $.DataAccess.list("users", "none");
            req2.success(function (json) {
                var utente = json;
                console.log("utente", utente);
                dfd.resolve(utente);
                // compileForm(data);
            });
            return dfd;

        }

        /*
        load table con le opzioni
        -----------------------------------------------------------*/
        function loadTable() {
            $('#GenericTable').DataTable( {
                responsive: true
            } );

        }
        /**************************************/
        /* update  lista di base
        /*************************************/
        $.fn.save = function () {
            $.when(   $.module.savedata() )
                .then(
                    function( data ) {
                        console.log("savedata", data);
                        $.module.update(data)
                    }
                );

        }
    }); //document ready




});




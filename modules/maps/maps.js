
$(function () {

    $(document).ready(function () {
        $("#backButtonTable").hide();
        //$("body").css("-ms-content-zooming", "none");
        //$("body").css("touch-action", "none");
        //$("html").css("-ms-content-zooming", "none");
        //$("body").css("touch-action", "none");


        //SIGNAL IR*************************************************************************//
        $.fn.device_changed = function (message) {
            //console.log("message", message);
            var type = message.deviceType;
            var installationId = message.installation;
            if (localStorage.installationId == installationId && localStorage.DeviceType == type) {
                updateDevice(message);
            }
        }

        function updateDevice(message) {

            console.log("marker collection",markersCollection );

            var findMarker = $.grep(markersCollection, function(e){ return e.id == message._id; });
            console.log(findMarker);

            console.log("findMarker",findMarker );

            Latitude = findMarker[0].data.geoLocalization.latitude;
            Longitude = findMarker[0].data.geoLocalization.longitude;
            if (Longitude == 0 || Latitude == 0 || Longitude == null || Latitude == null) {
                Latitude = impLat;
                Longitude = impLong;
                var latlng = new google.maps.LatLng(parseFloat(Latitude), parseFloat(Longitude));
            } else {
                var latlng = new google.maps.LatLng(parseFloat(Latitude), parseFloat(Longitude));
            }
            var Desc = findMarker[0].data.description;
            var Cod = findMarker[0].data.deviceType;
            if (findMarker[0].data.lastValueGranular.length > 0) {var stato = findMarker[0].data.lastValueGranular[0].value} else {var stato = 3};
            console.log("Stato", stato);

            var Id = findMarker[0].id;
            geocodefromLatLong(Latitude, Longitude, latlng, Cod, Id, Desc, findMarker[0].data, stato);



        }

        // FINE SIGNAL IR*************************************************************************//

        $("#backManage").on("click", function () {
            $.module.load('Impianti/Supervisors/ManageDevice8');
        });

        $("#backButtonTable").on("click", function () {
            $('#panel-body-SectionList').hide();
            $('#panel-body-ListPlants').show();
            $("#backButtonTable").hide();
            $('#panelLabel').html("Lampioni");
        });

        $("#Scontainter").draggable();
        var impLat = 0;
        var impLong = 0;
        var Latitude = 0;
        var Longitude = 0;
        var map;
        var elevator;
        var elevation = 0;
        var AddressOnMap = null;
        var geocoder = null;
        var shadow = null;
        var clickIcon = null;
        var clickMarker = null;
        var markers = [];
        var marker = null;
        var infowindow = null;
        var MAPFILES_URL = "http://maps.gstatic.com/intl/en_us/mapfiles/";
        var bounds;
        var numberPattern = /\d+/g;

        localStorage.setItem("draggabile", "0");


        //setbutton();

        elevator = new google.maps.ElevationService();
        geocoder = new google.maps.Geocoder();
        infowindow = new google.maps.InfoWindow();

        var $tabellaPresente = 0;
        var markersCollection = [];
        var markerSelected = [];

        var clevergygmarkerimage_verde = {
            url: 'plugins/images/markers_verde_vuoto.png',
            // This marker is 20 pixels wide by 32 pixels tall.
            size: new google.maps.Size(32, 37),
            // The origin for this image is 0,0.
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at 0,32.
            anchor: new google.maps.Point(0, 37)
        };

        var clevergygmarkerimage_giallo = {
            url: 'plugins/images/markers_giallo.png',
            // This marker is 20 pixels wide by 32 pixels tall.
            size: new google.maps.Size(32, 37),
            // The origin for this image is 0,0.
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at 0,32.
            anchor: new google.maps.Point(0, 37)
        };

        var clevergygmarkerimage_rosso = {
            url: 'plugins/images/markers_rosso_vuoto.png',
            // This marker is 20 pixels wide by 32 pixels tall.
            size: new google.maps.Size(32, 37),
            // The origin for this image is 0,0.
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at 0,32.
            anchor: new google.maps.Point(0, 37)
        };

        //Readhs();

        ReadImpianto_hs();

        function ReadImpianto_hs() {
            impLat = 45.45;
            impLong = 10.55;
            Latitude = 45.45;
            Longitude = 10.55;

            setTimeout(function () { loadmap(Latitude, Longitude); }, 1000);

        }

        function loadmap() {
            var h = $(window).height() - $('.header').height(); // - 30;
            //google maps

            $('#googlemap').height(h);
            map = new google.maps.Map(document.getElementById('googlemap'), {
                zoom: 18,
                panControl: true,
                zoomControl: true,
                mapTypeControl: true,
                scaleControl: true,
                overviewMapControl: true,
                center: new google.maps.LatLng(Latitude, Longitude),
                mapTypeId: google.maps.MapTypeId.HYBRID
            });
            bounds = new google.maps.LatLngBounds();


            $('#Scontainter').show();
            $("#Scontainter").animate({
                left: "100px",
                top: "200px",
                opacity: 1
            }, {
                duration: 500,
                queue: false
            });
            setTimeout(function () {  Read_componenti(); }, 1000);
        }

        $(window).resize(function () {
            var h = $(window).height() - $('.header').height() - 30;
            $('#googlemap').height(h);
        });

        /*
         map
         -----------------------------------------------*/
        $.fn.sidebar = function (Id) {
            localStorage.setItem('idLux', Id);
            loadSezione();

        }

        $.fn.Close_det = function () {
            $('#usermenu').empty();
        }

        function geocodefromLatLong(Latitude, Longitude, latlng, Cod, Id, Desc, data, stato) {

            console.log(Latitude, Longitude, latlng, Cod, Id, Desc, data, stato);

            if (localStorage.draggabile == "1") {
                if (stato == 0) {
                    DeleteMarker(Id);
                    marker = new google.maps.Marker({
                        position: latlng,
                        icon: clevergygmarkerimage_rosso,
                        label:   data.code.match( numberPattern).join(''),
                        map: map,
                        draggable: true,
                        type: Cod,
                        data: data,
                        id: Id
                    });
                    //console.log("marker",marker );
                    markersCollection.push(marker);
                } else if (stato == 1) {
                    DeleteMarker(Id);
                    marker = new google.maps.Marker({
                        position: latlng,
                        icon: clevergygmarkerimage_verde ,
                        label:   data.code.match( numberPattern).join(''),
                        map: map,
                        draggable: true,
                        type: Cod,
                        data: data,
                        id: Id
                    });
                    //console.log("marker",marker );
                    markersCollection.push(marker);


                } else {
                    DeleteMarker(Id);
                    marker = new google.maps.Marker({
                        position: latlng,
                        icon: clevergygmarkerimage_giallo,
                        label:   data.code.match( numberPattern).join(''),
                        map: map,
                        draggable: true,
                        type: localStorage.getItem("Cod"),
                        data: data,
                        id: Id
                    });
                    markersCollection.push(marker);
                }
            } else {
                if (stato == 0) {
                    DeleteMarker(Id);
                    marker = new google.maps.Marker({
                        position: latlng,
                        icon: clevergygmarkerimage_rosso,
                        label:   data.code.match( numberPattern).join(''),
                        map: map,
                        draggable: false,
                        type: Cod,
                        data: data,
                        id: Id
                    });
                    //console.log("marker",marker );
                    markersCollection.push(marker);
                } else if (stato == 1) {
                    DeleteMarker(Id);
                    marker = new google.maps.Marker({
                        position: latlng,
                        icon: clevergygmarkerimage_verde,
                        label:   data.code.match( numberPattern).join(''),
                        map: map,
                        labelClass: "labelsMia",
                        draggable: false,
                        type: Cod,
                        data: data,
                        id: Id
                    });
                    //console.log("marker",marker );
                    markersCollection.push(marker);


                } else {
                    DeleteMarker(Id);
                    marker = new google.maps.Marker({
                        position: latlng,
                        icon: clevergygmarkerimage_giallo,
                        label:   data.code.match( numberPattern).join(''),
                        map: map,
                        draggable: false,
                        type: localStorage.getItem("Cod"),
                        data: data,
                        id: Id
                    });
                    markersCollection.push(marker);


                }

            }// chiudo l'if che mi identifica se draggabile o meno

            //var content = setInfowindowContent(DesImpianto, Descr, Indirizzo, isOnline, MaintenanceMode, hsId);
            //se c'e un infowindow la chiudo se no vadoavanti ela creo
            if (infowindow) infowindow.close();
            infowindow = new google.maps.InfoWindow()
            google.maps.event.addListener(marker, 'click', function () {
                removeActiveClass();
                $('#li_' + this.Id).addClass("active");
                // console.log("MARKER", marker);
                localStorage.setItem("Cod", "STALL");
                localStorage.setItem("id_stall", Id);
                var Cod =  this.data.code;
                var description =    this.data.description;
                markerSelected.splice(0, 1);
                markerSelected.push(this);
                console.log("MARKER SELEZIONATO", markerSelected);


                infowindow.setContent(setInfowindowContent(Latitude, Longitude, latlng, Cod, description, Id));
                infowindow.open(map, this);

            });

            bounds.extend(latlng);
            map.fitBounds(bounds);
            map.setCenter(latlng);
            //CONTROLLO DEL DRAG AND DROP DI LAMPIONE O GATEWAY
            google.maps.event.addListener(marker, 'dragstart', function (event) {
                console.log('CODICE START=' + this.type);
                var lat = this.getPosition().lat();
                var lng = this.getPosition().lng();
                console.log('dragstart this.id=' + this.id + ' lat=' + lat + ' lng=' + lng + ' latlng=' + latlng + ' this.getPosition()=' + this.getPosition());
            });
            google.maps.event.addListener(marker, 'dragend', function (event) {
                console.log('CODICE END=' + this.id);
                var lat = this.getPosition().lat();
                var lng = this.getPosition().lng();
                var latlng = this.getPosition();


                $('#lat_' + this.id).text(lat);
                $('#long_' + this.id).text(lng);

                console.log("marker spostato",this.id );
                var savedata = {};
                savedata.geoLocalization={
                    "latitude":lat,
                    "longitude": lng
                };

                console.log("DATI SALVATI", savedata);
                var req = $.DataAccess.device_update(localStorage.installationId, this.id, savedata);
                req.success(function (json) {
                    if (json.retVal) {
                        console.log ("form salvato", json.dataObject);
                        $('.page-title').html(json.dataObject.description);
                        $.toast({
                            heading: 'Device Modificato',
                            // text: 'Installazione Modificata!',
                            position: 'top-left',
                            loaderBg:'#ff6849',
                            icon: 'info',
                            hideAfter: 3500,

                            stack: 6
                        })
                    } else {
                        $.toast({
                            heading: 'Device',
                            text: 'Dati non salvati, ricaricare la pagina e riprovare.',
                            position: 'top-left',
                            loaderBg:'#ff6849',
                            bgColor: "#ff0000",
                            icon: 'warning',
                            hideAfter: 3500,

                            stack: 6
                        })
                    }
                });


            });



        }




        function setInfowindowContent(Latitude, Longitude, latlng, Cod, description, Id) {

            var html = "";

            html = '<div id="content" style="display:block;white-space: nowrap;width:400px;">';
            //html += '<a style="color:#337ab7;" href="javascript:selPlant(' + Id + ',' + Cod + ');" id="impianto">' + Desc + '</a>';
            html += '<a style="color:#337ab7; font-weight:600" href="javascript:sidebar(' + Id + ');" class="open_detail">' + Cod +'</a>';
            html += '<div id="info_window"><br />';
            html += '<p><br />';

            html += '<b>Descrizione</b>: ' + description + '</p>';
            html += '</div>';
            html += '</div>';

            return html;

        }
        /*---------------------------------------------*/


        function DeleteMarker(id) {
            //Find and remove the marker from the Array
            //console.log("markersCollection", markersCollection);
            for (var i = 0; i < markersCollection.length; i++) {
                if (markersCollection[i].id == id) {
                    //Remove the marker from Map
                    markersCollection[i].setMap(null);
                    //Remove the marker from array.
                    markersCollection.splice(i, 1);
                    return;
                }
            }
        };

        function removeActiveClass() {
            $('#PlantList li').each(function (n) {
                $(this).removeClass("active");
            });
        }


        /*
         selezione dell elemento al click sulla lista
         ----------------------------------------------------------------*/


        $.fn.selPlant = function (Id, codice) {
            //prendo il codice dell elemento cliccato e creo il marker sulla mappa
            switch (codice) {
                case "Lamp":
                    initLamp(Id);
                    break;
                case "Sezione":
                    initGTW(Id);
                    break;
            }
        }

        /*--------------------------------------------------------------*/
        $('#close_list').on('click', function (e) {
            $.fn.Close_det();
            //Find and remove the marker from the Array
            for (var i = 0; i < markersCollection.length; i++) {
                markersCollection[i].setMap(null);
            }
            markersCollection = [];
            //Read_componenti();

        });

        function Read_componenti() {

            SetPark();
        }

        $("#panel-body-ListPlants").mousedown(function (event) {
            event.stopPropagation();
        });

        /*
         LAMPIONI    GEOLOC E INIZIALIZZAZIONE
         ----------------------------------------------------------------*/

        function SetPark() {
            $("#panel-body-ListPlants").empty();
            //localStorage.setItem("Cod", "Lamp");
            //parking

            localStorage.DeviceType = "STALL";
            var req = $.DataAccess.device_read_type(localStorage.installationId, localStorage.DeviceType);
            req.success(function (json) {
                //console.log("JSON", json);
                if (json.retVal == true) {
                    var data = json.dataObject;
                    console.log("Stalli", data);
                    var voci = {};
                    voci['data'] = data;
                    //devo mettere tutta la table nel template se no si duplica l'id
                    for (var i = 0; i < data.length; i++) {
                        Latitude = data[i].geoLocalization.latitude;
                        Longitude = data[i].geoLocalization.longitude;
                        if (Longitude == 0 || Latitude == 0 || Longitude == null || Latitude == null) {
                            Latitude = impLat;
                            Longitude = impLong;
                            var latlng = new google.maps.LatLng(parseFloat(Latitude), parseFloat(Longitude));
                        } else {
                            var latlng = new google.maps.LatLng(parseFloat(Latitude), parseFloat(Longitude));
                        }
                        var Desc = data[i].description;
                        var Cod = data[i].deviceType;
                        if (data[i].lastValueGranular.length > 0) {var stato = data[i].lastValueGranular[0].value;} else {var stato = 3};
                        console.log("Stato", stato);

                        var Id = data[i]._id;
                        geocodefromLatLong(Latitude, Longitude, latlng, Cod, Id, Desc, data[i], stato);
                    }//end for
                }
            });
        }

        function initLamp(Id) {
            var req = $.DataAccess.lux_read(Id);
            req.success(function (json) {
                //console.log("JSON", json);
                if (json.retval === true) {
                    var data = json.dataObject;

                    Latitude = data.Latitude;
                    Longitude = data.Longitude;
                    //console.log("Longitude",Longitude,"Latitude",Latitude);
                    if (Longitude == 0 && Latitude == 0) {


                        Latitude = impLat;
                        Longitude = impLong;
                        var latlng = new google.maps.LatLng(parseFloat(Latitude), parseFloat(Longitude));
                        console.log("NO",latlng);
                    } else {
                        var latlng = new google.maps.LatLng(parseFloat(data.Latitude), parseFloat(data.Longitude));

                    }
                    var Desc = data.description;
                    var Cod = data.deviceType;
                    if (data.lastValueGranular.length > 0) {var stato = data.lastValueGranular[0].value;} else {var stato = 3};

                    var Id = data._id;


                    //var latlng = new google.maps.LatLng(parseFloat(data[i].Latitude), parseFloat(data[i].Longitude));
                    geocodefromLatLong(Latitude, Longitude, latlng, Cod, Id, Desc, data, stato);

                    infowindow.setContent(setInfowindowContent(Latitude, Longitude, latlng, Cod, description));
                    infowindow.open(map, marker);
                }
            });
        }


        $.fn.setDrag = function (flag) {
            localStorage.setItem("draggabile", flag);
            if (localStorage.draggabile == 1) {
                var backflag = '0';
                $("#append_draggable").html('<a style="width:160px;" href="javascript:setDrag(' + backflag + ' ) ;" class=""><i class="fa fa-map-marker">&nbsp;</i><span name="lbl" caption="draggabile">Marker Draggabili</span></a>');
                loadmap();
            } else {
                var backflag = '1';
                $("#append_draggable").html('<a style="width:160px;" href="javascript:setDrag(' + backflag + ' ) ;" class=""><i class="fa fa-times">&nbsp;</i><span name="lbl" caption="draggable">Marker non Draggabili</span></a>');
                loadmap();
            }
        }


        /*******GOBACK*******************/
        $.fn.goBack = function (where) {
            $.module.load('Impianti/Operators/'+where);
        }
        /*******GOBACK*******************/



    }); // document ready

});


function selPlant(Id, codice) {
    $.fn.selPlant(Id, codice);
}


function setDrag(flag) {
    $.fn.setDrag(flag);
}


function goBack(where) {
    $.fn.goBack(where);
}

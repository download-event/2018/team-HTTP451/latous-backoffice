/**
 * Created by paolo on 10/09/2017.
 */
$(function () {

    $(document).ready(function () {

        //load menu di sinistra
        // $.module.loadMenu("admin");
        //google maps variabiles
        initMap();
        // $("#Scontainter").draggable();
        var $impLat = 45.45;
        var $impLong = 10.55;
        var $Latitude = 0;
        var $Longitude = 0;
        var map;
        var elevator;
        var elevation = 0;
        var AddressOnMap = null;
        var geocoder = null;
        var shadow = null;
        var clickIcon = null;
        var clickMarker = null;
        var markers = [];
        var marker = null;
        var infowindow = null;
        var MAPFILES_URL = "http://maps.gstatic.com/intl/en_us/mapfiles/";
        var bounds;
        var numberPattern = /\d+/g;
        //daggable marker
        localStorage.setItem("draggabile", "0");

        //settiamo i marker
        elevator = new google.maps.ElevationService();
        geocoder = new google.maps.Geocoder();
        infowindow = new google.maps.InfoWindow();

        var $tabellaPresente = 0;
        var $markersCollection = [];
        var $markerSelected = [];

        var clevergygmarkerimage_verde = {
            url: 'plugins/images/markers_verde.png',
            // This marker is 20 pixels wide by 32 pixels tall.
            size: new google.maps.Size(32, 37),
            // The origin for this image is 0,0.
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at 0,32.
            anchor: new google.maps.Point(0, 37)
        };

        var clevergygmarkerimage_giallo = {
            url: 'plugins/images/markers_giallo.png',
            // This marker is 20 pixels wide by 32 pixels tall.
            size: new google.maps.Size(32, 37),
            // The origin for this image is 0,0.
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at 0,32.
            anchor: new google.maps.Point(0, 37)
        };

        var clevergygmarkerimage_rosso = {
            url: 'plugins/images/markers_rosso_vuoto.png',
            // This marker is 20 pixels wide by 32 pixels tall.
            size: new google.maps.Size(32, 37),
            // The origin for this image is 0,0.
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at 0,32.
            anchor: new google.maps.Point(0, 37)
        };



        function initMap(){
             $impLat = 45.45;
             $impLong = 10.55;
             $Latitude = 45.464204;
             $Longitude = 9.189982;
            setTimeout(function () { loadmap($Latitude, $Longitude); }, 1000);
        }

        function loadmap(Latitude,Longitude) {
            var h = $(window).height() - $('.header').height(); // - 30;
            // var latlong = new google.maps.LatLng(Latitude, Longitude);

            //google maps
            $('#googlemap').height(h);
            map = new google.maps.Map(document.getElementById('googlemap'), {
                zoom: 18,
                panControl: true,
                zoomControl: true,
                mapTypeControl: true,
                scaleControl: true,
                overviewMapControl: true,
                center: new google.maps.LatLng(Latitude, Longitude),

            });
            bounds = new google.maps.LatLngBounds();
            console.log("bounds-create", bounds);

            // $('#Scontainter').show();
            // $("#Scontainter").animate({
            //     left: "100px",
            //     top: "200px",
            //     opacity: 1
            // }, {
            //     duration: 500,
            //     queue: false
            // });
            setTimeout(function () {
                $.when( loadProject2() ).then(
                    function( status ) {

                        // setTimeout(function () {
                        //     loadTable();
                        //     // initMap();
                        //     // $.module.checkPrivileges();
                        // }, 500);


                    }
                );
            }, 1000);
        }

        $(window).resize(function () {
            var h = $(window).height() - $('.header').height() - 30;
            $('#googlemap').height(h);
        });



        function geocodefromLatLong(Latitude, Longitude, latlng,  Id, name, data, stato) {

            console.log("Latitude", Latitude,"Longitude", Longitude, latlng,  Id, name,"data",  data, stato);

            if (localStorage.draggabile == "1") {
                if (stato == "Aperta") {
                    DeleteMarker(Id);
                    marker = new google.maps.Marker({
                        position: latlng,
                        icon: clevergygmarkerimage_rosso,
                        label:    "",
                        map: map,
                        draggable: true,
                        type: name,
                        data: data,
                        id: Id
                    });
                    //console.log("marker",marker );
                    $markersCollection.push(marker);
                } else if (stato == "Risolta") {
                    DeleteMarker(Id);
                    marker = new google.maps.Marker({
                        position: latlng,
                        icon: clevergygmarkerimage_verde ,
                        label:    "",
                        map: map,
                        draggable: true,
                        type: name,
                        data: data,
                        id: Id
                    });
                    //console.log("marker",marker );
                    $markersCollection.push(marker);


                } else {
                    DeleteMarker(Id);
                    marker = new google.maps.Marker({
                        position: latlng,
                        icon: clevergygmarkerimage_giallo,
                        label:    "",
                        map: map,
                        draggable: true,
                        type: name,
                        data: data,
                        id: Id
                    });
                    $markersCollection.push(marker);
                }
            } else {
                if (stato == "Aperta") {
                    DeleteMarker(Id);
                    marker = new google.maps.Marker({
                        position: latlng,
                        icon: clevergygmarkerimage_rosso,
                        label:   "",
                        map: map,
                        draggable: false,
                        type:name,
                        data: data,
                        id: Id
                    });
                    //console.log("marker",marker );
                    $markersCollection.push(marker);
                } else if (stato == "Risolta") {
                    DeleteMarker(Id);
                    marker = new google.maps.Marker({
                        position: latlng,
                        icon: clevergygmarkerimage_verde,
                        label:   "",
                        map: map,
                        labelClass: "labelsMia",
                        draggable: false,
                        type: name,
                        data: data,
                        id: Id
                    });
                    //console.log("marker",marker );
                    $markersCollection.push(marker);


                } else {
                    DeleteMarker(Id);
                    marker = new google.maps.Marker({
                        position: latlng,
                        icon: clevergygmarkerimage_giallo,
                        label:   "",
                        map: map,
                        draggable: false,
                        type: name,
                        data: data,
                        id: Id
                    });
                    $markersCollection.push(marker);


                }

            }// chiudo l'if che mi identifica se draggabile o meno

            //var content = setInfowindowContent(DesImpianto, Descr, Indirizzo, isOnline, MaintenanceMode, hsId);
            //se c'e un infowindow la chiudo se no vadoavanti ela creo
            if (infowindow) infowindow.close();
            infowindow = new google.maps.InfoWindow();
            google.maps.event.addListener(marker, 'click', function () {
                // removeActiveClass();
                // $('#li_' + this.Id).addClass("active");
                console.log("MARKER", marker);
                // localStorage.setItem("Cod", "STALL");
                localStorage.setItem("id_stall", Id);
                // var Cod =  this.data.code;
                var name =    this.data.category;
                var notes =    this.data.description;
                $markerSelected.splice(0, 1);
                $markerSelected.push(this);
                console.log("MARKER SELEZIONATO", notes);


                infowindow.setContent(setInfowindowContent(Latitude, Longitude, latlng, name, Id, notes));
                infowindow.open(map, this);

            });
            console.log("bounds", bounds);
            bounds.extend(latlng);
            map.fitBounds(bounds);
            map.setCenter(latlng);
            //CONTROLLO DEL DRAG AND DROP DI LAMPIONE O GATEWAY
            google.maps.event.addListener(marker, 'dragstart', function (event) {
                console.log('CODICE START=' + this.type);
                var lat = this.getPosition().lat();
                var lng = this.getPosition().lng();
                console.log('dragstart this.id=' + this.id + ' lat=' + lat + ' lng=' + lng + ' latlng=' + latlng + ' this.getPosition()=' + this.getPosition());
            });
            google.maps.event.addListener(marker, 'dragend', function (event) {
                console.log('CODICE END=' + this.id);
                var lat = this.getPosition().lat();
                var lng = this.getPosition().lng();
                var latlng = this.getPosition();


                $('#lat_' + this.id).text(lat);
                $('#long_' + this.id).text(lng);

                console.log("marker spostato",this.id );
                var savedata = {};
                savedata.geoLocalization={
                    "latitude":lat,
                    "longitude": lng
                };

                console.log("DATI SALVATI", savedata);
                var req = $.DataAccess.device_update(localStorage.installationId, this.id, savedata);
                req.success(function (json) {
                    if (json.retVal) {
                        console.log ("form salvato", json.dataObject);
                        $('.page-title').html(json.dataObject.description);
                        $.toast({
                            heading: 'Device Modificato',
                            // text: 'Installazione Modificata!',
                            position: 'top-left',
                            loaderBg:'#ff6849',
                            icon: 'info',
                            hideAfter: 3500,

                            stack: 6
                        })
                    } else {
                        $.toast({
                            heading: 'Device',
                            text: 'Dati non salvati, ricaricare la pagina e riprovare.',
                            position: 'top-left',
                            loaderBg:'#ff6849',
                            bgColor: "#ff0000",
                            icon: 'warning',
                            hideAfter: 3500,

                            stack: 6
                        })
                    }
                });


            });



        }

        function DeleteMarker(id) {
            //Find and remove the marker from the Array
            //console.log("$markersCollection", $markersCollection);
            for (var i = 0; i < $markersCollection.length; i++) {
                if ($markersCollection[i].id == id) {
                    //Remove the marker from Map
                    $markersCollection[i].setMap(null);
                    //Remove the marker from array.
                    $markersCollection.splice(i, 1);
                    return;
                }
            }
        };


        function setInfowindowContent(Latitude, Longitude, latlng, name,  Id, notes) {

            var html = "";

            html = '<div id="content" style="display:block;white-space: nowrap;width:200px;">';
            //html += '<a style="color:#337ab7;" href="javascript:selPlant(' + Id + ',' + Cod + ');" id="impianto">' + Desc + '</a>';
            html += '<a style="color:#337ab7; font-weight:600" href="javascript:sidebar(' + Id + ');" class="open_detail">' + name +'</a>';
            html += '<div id="info_window">';
            html += '<p><br />';
            html += '<b>' + notes + '</b></p>';
            html += '</div>';
            html += '</div>';

            return html;

        }
        /*---------------------------------------------*/


        $.fn.sidebar = function (Id) {
           $.module.load('edit')
        }

        /*
         List of projects
         -----------------------------------------------------------*/
        function loadProject2() {

            var dfd = jQuery.Deferred();
            $("#listbody").empty();
            //metto il titolo alla pagina e controllo per clienti
            var pageTitle =  localStorage.modulo.toUpperCase() ;
            var template = localStorage.modulo;
            $('.page-title').text(pageTitle);

            //Setto e controllo i params


            //chiamo il server e recupero gli oggetti
            var req = $.DataAccess.list('issues', 'none' );
            req.success(function (json) {
                var data = json;
                //carico il menu
                $.module.loadMenu("default");
                //renderizzo i dati nei template
                console.log ("marker per mappa", data);

                    dataTemp = {"clienti":data};
                    // $( "#listbody" ).append(
                    //     $( "#"+template+"tutto" ).render( dataTemp, {formatUntil: function (until) {
                    //             return  moment(until).format("DD-MM-YYYY HH:mm");
                    //         }} )
                    //
                    // );

                //devo mettere tutta la table nel template se no si duplica l'id
                for (var i = 0; i < data.length; i++) {
                    Latitude = data[i].coordinates[1];
                    Longitude = data[i].coordinates[0];
                    if (Longitude == 0 || Latitude == 0 || Longitude == null || Latitude == null) {
                        Latitude = 0;
                        Longitude = 0;
                        var latlng = new google.maps.LatLng(parseFloat(Latitude), parseFloat(Longitude));
                    } else {
                        var latlng = new google.maps.LatLng(parseFloat(Latitude), parseFloat(Longitude));
                    }
                    var name = data[i].name;
                    var stato = data[i].status;

                    console.log("Stato>>>>>>>>>>>>>>>>>", stato);

                    var Id = data[i]._id;
                    geocodefromLatLong(Latitude, Longitude, latlng,  Id, name, data[i], stato);
                }//end for

                dfd.resolve();
            });//fine req

		}
        function loadTable() {
            //Order setting based on module
            // var sorting;
            // if (localStorage.modulo == "clienti" && localStorage.isCliente == 1) sorting = [[ 2, 'asc' ]];
            // else if (localStorage.modulo == "clienti" && localStorage.isCliente == 0) sorting = [[ 3, 'asc' ]];
            // else sorting = [[ 1, 'asc' ]];
            //numero colonne in base a ruolo
            var aoColumns = [];

            $('#GenericTable').DataTable( {

                columnDefs: [
                    { targets: [0, 1], visible: true},
                    { targets: '_all', visible: true }
                ],
                bPaginate: true,
                bFilter: true,
                responsive: true,
                bInfo: true,
                pageLength: 100,
                // "order":sorting
                // buttons: [
                //     'copy', 'excel', 'csv'
                // ]
            });

        }



        /*******SELECT A SINGOLO*******************/
        $.fn.edit = function (id, module) {
            localStorage.setItem("modulo", module);
            localStorage.setItem("id", id)
            $.module.load('anagrafica/edit');
        }
        /**************************/
		/*******CREATE A SINGOLO*******************/
        $.fn.edit = function (id, module) {
            localStorage.setItem("modulo", module);
            localStorage.setItem("id", id)
            $.module.load('anagrafica/create');
        }
        /**************************/
		
        /*******Delete PROJECT*******************/
        $.fn.del = function (id) {

            //Confirmation
            swal({
                title: "Sei sicuro?",
                text: "L'elemento verrà cancellato definitivamente",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function(){
                var req = $.DataAccess.general(localStorage.modulo, "delete", id);
				req.success(function (json) {
					console.log ("deleted data", JSON.parse(json));
					$("#"+id+"Row").fadeOut();
					$.toast({
                        heading: "Elemento di " +localStorage.modulo.toUpperCase() + " Cancellato",
                        text: '',
                        position: 'top-left',
                        loaderBg:'#ff6849',
                        icon: 'info',
                        hideAfter: 3500,

                        stack: 6
                    });

				});
			})
                    
             
        }
        /**************************/



    }); //document ready

});



function sel(id, module) {
    $.fn.sel(id, module);
}
function edit(id,module) {
    $.fn.edit(id, module);
}
function del(id) {
    $.fn.del(id);

}

$.templates("ticketsList",
    `<div class="col-sm-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <span class="page-title">LISTA DEI TICKETS</span>
			<a href="javascript: $.module.load('create','tickets');" ><i style="font-size: 21px;vertical-align: -4px;" class="icon-plus"></i></a>
            <div class="pull-right">
                <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                <!-- <a href="javascript:goBack();" data-perform=""><i class="ti-close"></i></a> -->
            </div>
        </div>
        <div class="panel-wrapper collapse in" aria-expanded="true">
            <div  class="panel-body">

                    <table style="font-size:14px;" id="GenericTable" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Titolo</th>
                                <th>Descrizione</th>
                                <th>Dipartimento</th>
                                <th>Stato</th>
                                 <th>Segnalazioni Correlate</th>
                                <th>Azioni</th>
                            </tr>
                        </thead>
                        <tbody>
                         {{for  items  }}
                             <tr id='{{:id}}'>                       
                                 <td> {{:title onerror=''}}</td>
                                                    
                                 <td> {{:description  onerror=''}}</td>
                                 <td> {{:department  onerror=''}}</td>
                                 <td> 
                                         {{if status == "Nuovo"}}
                                              <span class="badge badge-pill badge-danger float-right"> {{:status    onerror=''}}</span>
                                             
                                        {{else  status == "Preso in carico"}}
                                               <span class="badge badge-pill badge-info float-right"> {{:status    onerror=''}}</span>
                                        {{else  status == "Risolto"}}
                                      
                                                <span class="badge badge-pill badge-success float-right"> {{:status    onerror=''}}</span>
                                        {{/if}}                                                 
                                 </td>
                                 <td>
                                   <span class="badge badge-pill badge-inverse float-right">{{:issueIds.length onerror=''}} </span>
                                 </td>
                                 <td>
                                    <a  href="javascript: $rootingStore.updateState(function(state) {state.id = '{{:_id}}';});  $.module.load('edit','tickets');" class="btn btn-outline btn-info btn-xs editButton"  ><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a data-role='admin'  href="javascript:$.module.del('tickets','{{:_id}}');" class="btn btn-outline btn-danger btn-xs btn-1c deleteButton" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                         {{/for}}
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>`
);


$.templates("ticketsEdit",
    `<div class="col-md-12">
        <div class="panel panel-inverse">
             {{if items == "create"}}
                 <div class="panel-heading"> <span name="lbl" caption="anagraficalead">Crea Ticket</span>
                    <div class="pull-right">
                        <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                        <a href="javascript:$.module.goBack();" data-perform=""><i class="ti-close"></i></a>
    
                    </div>
                </div>
												   
			 {{else items == "edit"}}
                 <div class="panel-heading"> <span name="lbl" caption="anagraficalead"> Modifica Ticket</span>
                    <div class="pull-right">
                        <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                        <a href="javascript:$.module.goBack();" data-perform=""><i class="ti-close"></i></a>
    
                    </div>
                </div>											    
			 {{/if}}          
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <form action="#" class="form-horizontal" id='leadForm'>
                        <div class="form-body">
                            <!--row-->
                            <h3 class="box-title">Informazioni Generali </h3>
                            <hr class="m-t-0 m-b-40">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Titolo</label>
                                        <div class="col-md-9">
                                            <input id="title" style="text-transform:uppercase" name="title" type="text" class="form-control takeIn" placeholder="" required>
                                            <!-- <span class="help-block"> This field has error. </span> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Descrizione</label>
                                        <div class="col-md-9">
                                            <input id="description" style="text-transform:uppercase" name="description" type="text" class="form-control takeIn" placeholder="" required>
                                            <!-- <span class="help-block"> primo nome</span>  -->
											</div>
                                    </div>
                                </div>
	
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Dipartimento</label>
                                        <div class="col-md-9">
                                            <select id="department" name="department" type="text" class="form-control takeIn" placeholder="">
                                                <option value="Lavori pubblici">Lavori pubblici</option>
                                                <option value="Innovazione">Innovazione</option>
                                                <option value="Sport">Sport</option>
                                                <option value="Politiche per lo sviluppo">Politiche per lo sviluppo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
							
							                          
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Note</label>
										<div class="col-md-9">
											<textarea id="note" name="note" class="form-control takeIn" rows="5"></textarea>
											<!-- <span class="help-block"> This field has error. </span>  -->
										</div>

									</div>
								</div>
							
							</div>
                            <!--/row-->
                            <!--row-->
							<h3 class="box-title">Stato della Segnalazione</h3>
                            <hr class="m-t-0 m-b-40">
                                <div class="row">
                                     <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Stato</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <select  id='status' class="form-control takeIn esitoLead" name='status'>                                              
                                                        <option value="Nuovo">Nuovo</option>
                                                        <option value="Preso in carico">Preso in carico</option>
                                                        <option value="Risolto">Risolto</option>                                              
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                     </div>                                                           
                                </div>
                       
                            <!--/row-->
                            <!--row-->
                            <h3 class="box-title">Segnalazioni Correlate</h3>
                            <hr class="m-t-0 m-b-40">
                            {{if items == "create"}}
                                <ul id="listIssues">
                            
                                </ul>						
						    {{else items == "edit"}}
						        <ul id="listIssues">
						           {{if  data.issues}}
						              {{for data.issues}}
                                            <p data-id="{{:_id}}"  class="takeIn2"  >{{:category}} - {{:description}}</p>
                                      {{/for}}     
                                   {{else}}
                                        <p>nessuna segnalazione correlata</p>  
                                   {{/if}}                       
                                </ul>											    	
						    {{/if}}		
                            <!--/row-->
                            <div class="form-actions">
                                <div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
												    {{if items == "create"}}
												    	<a href="javascript: $.fn.create();" type="button" class="btn btn-success">Crea</a>
												    {{else items == "edit"}}
												    	<a href="javascript: $.fn.save();" type="button" class="btn btn-success">Salva</a>
												    	<a href="javascript:$.module.del();" data-role='admin' id="delBtn2" type="button" class="btn btn-default">Cancella</a>
												    	<a href="javascript:$.fn.addIssues();"   type="button" class="btn btn-default">Assegna </a>

												    {{/if}}										
												</div>
											</div>
										</div>
										<div class="col-md-6">

										</div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>`
);

$.templates("ticketsRow",
    ` <td> {{:title onerror=''}}</td>
                                                    
     <td> {{:description  onerror=''}}</td>
     <td> {{:department  onerror=''}}</td>
     <td> 
             {{if status == "Nuovo"}}
                  <span class="badge badge-pill badge-danger float-right"> {{:status    onerror=''}}</span>
                 
            {{else  status == "Preso in carico"}}
                   <span class="badge badge-pill badge-info float-right"> {{:status    onerror=''}}</span>
            {{else  status == "Risolto"}}
          
                    <span class="badge badge-pill badge-success float-right"> {{:status    onerror=''}}</span>
            {{/if}}                                                 
     </td>
     <td>
       <span class="badge badge-pill badge-inverse float-right">{{:issueIds.length onerror=''}} </span>
     </td>
     <td>
        <a  href="javascript: $rootingStore.updateState(function(state) {state.id = '{{:_id}}';});  $.module.load('edit','tickets');" class="btn btn-outline btn-info btn-xs editButton"  ><i class="fa fa-pencil" aria-hidden="true"></i></a>
        <a data-role='admin'  href="javascript:$.module.del('tickets','{{:_id}}');" class="btn btn-outline btn-danger btn-xs btn-1c deleteButton" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>
    </td>`
);

$.templates("ticketsListIssues",
    `<li>
        <p  data-id="{{:_id}}"  class="takeIn2"  >{{:category}} - {{:description}}</p>
    </li>`
);

$.templates("ticketsModal",
    `{{for items}}
    <li>
        <a href="javascript:$.fn.addIssuesRow('{{:_id}}' , '{{:description}}','{{:category}}');"   class="takeIn2"  >{{:description}} - Accuracy >> {{:similarity}}</a>
    </li>
    {{/for}}`
);
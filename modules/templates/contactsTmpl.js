$.templates("contactsList",
    `<div class="col-sm-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <span class="page-title">LISTA CONTATTI</span>
			<a href="javascript: $.module.load('create','contacts');" ><i style="font-size: 21px;vertical-align: -4px;" class="icon-plus"></i></a>
            <div class="pull-right">
                <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                <!-- <a href="javascript:goBack();" data-perform=""><i class="ti-close"></i></a> -->
            </div>
        </div>
        <div class="panel-wrapper collapse in" aria-expanded="true">
            <div  class="panel-body">

                    <table style="font-size:14px;" id="GenericTable" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>

                                <th>Nome</th>
                                <th>Cognome</th>
                                <th>Ruolo</th>
                                <th>Mail</th>
                                <th>Azioni</th>
                            </tr>
                        </thead>
                        <tbody>
                         {{for  items  }}
                             <tr id='{{:id}}'>

                                 <td>{{:firstName onerror=''}}</td>
                                 <td> {{:lastName onerror=''}}</td>
                                 <td> {{:roleInTheClientCompany     onerror=''}}</td>
                                 <td> {{:contact.email  onerror=''}}</td>
                                 <td>
                                    <a  href="javascript: $rootingStore.updateState(function(state) {state.id = '{{:_id}}';});  $.module.load('edit','contacts');" class="btn btn-outline btn-info btn-xs editButton"  ><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a data-role='admin'  href="javascript:$.module.del('contacts','{{:_id}}');" class="btn btn-outline btn-danger btn-xs btn-1c deleteButton" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                         {{/for}}
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>`
);


$.templates("contactsEdit",
    ``
);

$.templates("contactsRow",
    `<td>{{:clientCode onerror=''}}</td>
         <td> {{:companyId onerror=''}}</td>
         <td> {{:group     onerror=''}}</td>
         <td> {{:category  onerror=''}}</td>
         <td> {{:status    onerror=''}}</td>
         <td>
            <a  href="javascript: $rootingStore.updateState(function(state) {state.id = '{{:_id}}';});  $.module.load('edit','contacts');" class="btn btn-outline btn-info btn-xs editButton"  ><i class="fa fa-pencil" aria-hidden="true"></i></a>
            <a data-role='admin'  href="javascript:$.module.del('contacts','{{:_id}}');" class="btn btn-outline btn-danger btn-xs btn-1c deleteButton" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>
        </td>`
);

$.templates("contactsSample",
    ``
);
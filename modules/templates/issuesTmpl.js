$.templates("issuesList",
    `<div class="col-sm-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <span class="page-title">LISTA DELLE SEGNALAZIONI</span>
			<a href="javascript: $.module.load('create','issues');" ><i style="font-size: 21px;vertical-align: -4px;" class="icon-plus"></i></a>
            <div class="pull-right">
                <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                <!-- <a href="javascript:goBack();" data-perform=""><i class="ti-close"></i></a> -->
            </div>
        </div>
        <div class="panel-wrapper collapse in" aria-expanded="true">
            <div  class="panel-body">

                    <table style="font-size:14px;" id="GenericTable" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Categoria</th>
                                <th>N°Segnalato</th>
                                <th>Source</th>
                                <th>Descrizione</th>
                                <th>Stato</th>
                                <th>Azioni</th>
                            </tr>
                        </thead>
                        <tbody>
                         {{for  items  }}
                             <tr id='{{:id}}'>                       
                                 <td> {{:category onerror=''}}</td>
                                  <td> 
                                         <span class="badge badge-pill badge-inverse float-right">{{:plusOnes  onerror=''}} </span>
                                  </td>
                                 <td> {{:source     onerror=''}}</td>
                                 <td> {{:description  onerror=''}}</td>
                                 <td> 
                                         {{if status == "Aperta"}}
                                              <span class="badge badge-pill badge-danger float-right"> {{:status    onerror=''}}</span>
                                             
                                        {{else  status == "Presa in carico"}}
                                               <span class="badge badge-pill badge-info float-right"> {{:status    onerror=''}}</span>
                                        {{else  status == "Risolta"}}
                                      
                                                <span class="badge badge-pill badge-success float-right"> {{:status    onerror=''}}</span>
                                        {{/if}}                                                 
                                 </td>
                                 <td>
                                    <a  href="javascript: $rootingStore.updateState(function(state) {state.id = '{{:_id}}';});  $.module.load('edit','issues');" class="btn btn-outline btn-info btn-xs editButton"  ><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <a data-role='admin'  href="javascript:$.module.del('issues','{{:_id}}');" class="btn btn-outline btn-danger btn-xs btn-1c deleteButton" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    <a data-role='admin'   href="javascript:localStorage.setItem('issueForTicketid', '{{:id}}'); $.module.load('createExt','tickets');" class="btn btn-outline btn-primary btn-xs btn-1c addTicket" ><i class="fa fa-plus" aria-hidden="true"></i></a>

                                </td>
                            </tr>
                         {{/for}}
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>`
);


$.templates("issuesEdit",
    `<div class="col-md-12">
        <div class="panel panel-inverse">
             {{if items == "create"}}
                 <div class="panel-heading"> <span name="lbl" caption="anagraficalead">Crea Seganalazione</span>
                    <div class="pull-right">
                        <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                        <a href="javascript:$.module.goBack();" data-perform=""><i class="ti-close"></i></a>
    
                    </div>
                </div>
												   
			 {{else items == "edit"}}
                 <div class="panel-heading"> <span name="lbl" caption="anagraficalead"> Modifica Segnalazione</span>
                    <div class="pull-right">
                        <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                        <a href="javascript:$.module.goBack();" data-perform=""><i class="ti-close"></i></a>
    
                    </div>
                </div>											    
			 {{/if}}          
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <form action="#" class="form-horizontal" id='leadForm'>
                        <div class="form-body">
                            <!--row-->
                            <h3 class="box-title">Informazioni Generali </h3>
                            <hr class="m-t-0 m-b-40">
                            <div class="row">
                          
                                <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Categoria</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <select  id='category' class="form-control takeIn " name='category'>
                                                        <option value="Buca">Buca</option>
                                                        <option value="Lampione">Lampione</option>
                                                        <option value="BlackOut">BlackOut</option>     
                                                         <option value="Scarico abusivo di rifiuti">Scarico abusivo di rifiuti</option>
                                                        <option value="Altro">Altro</option>
                                                                                                  
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Descrizione</label>
                                        <div class="col-md-9">
                                            <input id="description" style="text-transform:uppercase" name="description" type="text" class="form-control takeIn" placeholder="" required>
                                            <!-- <span class="help-block"> primo nome</span>  -->
											</div>
                                    </div>
                                </div>
	
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nome Autore</label>
                                        <div class="col-md-9">
                                            <input id="['first name']" name="['first name']" type="text" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Cognome autore</label>
                                        <div class="col-md-9">
                                            <input id="['last name']" name="['last name']" type="text" class="form-control " disabled>
                                        </div>
                                    </div>
                                </div>								                        
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-3">Note</label>
										<div class="col-md-9">
											<textarea id="annotations" name="annotations" class="form-control takeIn" rows="5"></textarea>
											<!-- <span class="help-block"> This field has error. </span>  -->
										</div>

									</div>
								</div>
								<div class="col-md-6" data-block='admin'>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Data Inserimento</label>
                                        <div class="col-md-9" data-block='admin'>
                                            <input id="createdAt" name="createdAt" type="text" class="form-control takeIn generalTimePicker" disabled >
                                        </div>
                                    </div>
								</div>
							</div>
                            <!--/row-->
                            <!--row-->
							<h3 class="box-title">Stato della Segnalazione</h3>
                            <hr class="m-t-0 m-b-40">
                                <div class="row">
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Stato</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <select  id='status' class="form-control takeIn " name='status'>
                                                        <option value="Aperta">Aperta</option>
                                                        <option value="Presa in carico">Presa in Carico</option>
                                                        <option value="Risolta">Risolta</option>                                              
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                     </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Source</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <select  id='source' class="form-control takeIn " name='source'>
                                                        <option value="APP">APP</option>
                                                        <option value="FACEBOOK_BOT">FACEBOOK_BOT</option>
                                                        <option value="OTHER">OTHER</option>                                              
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                     </div>                            
                                </div>
                   
                            <!--/row-->
                               <!--row-->
                            <h3 class="box-title">Parole Chiave dell'Utente </h3>
                            <hr class="m-t-0 m-b-40">
                            <div class="row">                                                      
                                  <div class="col-md-12">
                                      {{props data.userInsertedKeywords}}
                                         <button type="button" class="btn btn-outline btn-primary  btn-1c">{{:prop}}</button>
                                      {{/props}}
                                  <div>
                             </div>
                             <br>
                            <!--/row-->
                           
                            <!--row-->
                            <h3 class="box-title">Foto Allegate</h3>
                            <hr class="m-t-0 m-b-40">
                            <div class="row">                                                      
                                  <div class="col-md-12">
                                    {{if data.pictureUrl }}
                                    <img src="{{:data.pictureUrl}}">  
                                    {{else}}
                                     <p>Nessuna immagine è stata inviata dall utente</p>
                                    {{/if}}                            
                                  <div>
                             </div>
                    
                            <!--/row-->
                      
							 <hr class="m-t-0 m-b-40">
                            <div class="form-actions">
                                <div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
												    {{if items == "create"}}
												    	<a href="javascript: $.fn.create();" type="button" class="btn btn-success">Crea</a>
												    {{else items == "edit"}}
												    	<a href="javascript: $.fn.save();" type="button" class="btn btn-success">Salva</a>
												    	<a href="javascript:$.module.del();" data-role='admin' id="delBtn2" type="button" class="btn btn-default">Cancella</a>
												    {{/if}}
													
												</div>
											</div>
										</div>
									
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>`
);

$.templates("issuesRow",
    ` <td> {{:category onerror=''}}</td>
      <td> 
             <span class="badge badge-pill badge-inverse float-right">{{:plusOnes  onerror=''}} </span>
      </td>
     <td> {{:source     onerror=''}}</td>
     <td> {{:description  onerror=''}}</td>
     <td> 
             {{if status == "Aperta"}}
                  <span class="badge badge-pill badge-danger float-right"> {{:status    onerror=''}}</span>
                 
            {{else  status == "Presa in carico"}}
                   <span class="badge badge-pill badge-info float-right"> {{:status    onerror=''}}</span>
            {{else  status == "Risolta"}}
          
                    <span class="badge badge-pill badge-success float-right"> {{:status    onerror=''}}</span>
            {{/if}}                                                 
     </td>
     <td>
        <a  href="javascript: $rootingStore.updateState(function(state) {state.id = '{{:_id}}';});  $.module.load('edit','issues');" class="btn btn-outline btn-info btn-xs editButton"  ><i class="fa fa-pencil" aria-hidden="true"></i></a>
        <a data-role='admin'  href="javascript:$.module.del('issues','{{:_id}}');" class="btn btn-outline btn-danger btn-xs btn-1c deleteButton" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>
        <a data-role='admin'   href="javascript:localStorage.setItem('issueForTicketid', '{{:id}}'); $.module.load('createExt','tickets');" class="btn btn-outline btn-primary btn-xs btn-1c addTicket" ><i class="fa fa-plus" aria-hidden="true"></i></a>

    </td>`
);


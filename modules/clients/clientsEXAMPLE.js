/**
 * Created by paolo on 10/09/2017.
 */
$(function () {

    $(document).ready(function () {
        $.module.loadMenu("default");
        /**************************************/
        /* list  form di base*/
        /*************************************/
        var params = {"fields":["id","clientCode","companyId","group","category","status"]}
        // $.when(   $.module.list(localStorage.modulo, params) ).done(
        //     function( data ) {
        //         console.log("clients", data);
        //         //caso particolare delle liste le gestisco tutto qui. manipolazioni di dati
        //         $( "#listHolder" ).append(
        //             $( "#"+localStorage.modulo+"List" ).render( {"items":data} )
        //         );
        //     }
        // );

        //PROMESSA BASE PER CONCATENARE DUE FUNZIONI
        $.when(   $.module.list(localStorage.modulo, params) )
            .then(function (data) {
                console.log("data prima funione", data);
                // return loadUsers();
                return $.when( data, loadUsers() )
        }).then(
            function( data , users ) {
                console.log("data", data, "utente", users);
                //caso particolare delle liste le gestisco tutto qui. manipolazioni di dati
                $( "#listHolder" ).append(
                    $( "#"+localStorage.modulo+"List" ).render( {"items":data} )
                );
            }
        );


        /*
          List file for servizi
          -----------------------------------------------------------*/
        function loadUsers() {
            var dfd = jQuery.Deferred();
            var req2 = $.DataAccess.list("users", "none");
            req2.success(function (json) {
                var utente = json;
                console.log("utente", utente);
                dfd.resolve(utente);
                // compileForm(data);
            });
            return dfd;

        }


        /**************************************/
        /* update  lista di base
        /*************************************/
        // $.when(   formatData() ).done(
        //     function( data ) {
        //         console.log("dati da salvare", data);
        //         //caso particolare delle edit le gestisco tutto qui.
        //         $.module.savedata(localStorage.modulo, localStorage.id, data);
        //
        //     }
        // );
        //
        // function formatData() {
        //     //formattazione e slavataggio dei dati prima di inviarli alla funzione che fa solo l'update
        // }
    }); //document ready

});




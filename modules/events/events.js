/**
 * Created by paolo on 10/09/2017.
 */
$(function () {
    $(document).ready(function () {
        $.module.loadMenu($userStore.getState().role);
        /**************************************/
        /* list  form di base*/
        /*************************************/
        var params = {"fields":["id","clientCode","companyId","group","category","status"]}
        $.when(   $.module.list($rootingStore.getState().modulo, params) )
           .then(
            function( data ) {
                console.log("data", data);
                //caso particolare delle liste le gestisco tutto qui. manipolazioni di dati
                $( "#listHolder" ).append(
                    $( "#"+$rootingStore.getState().modulo +"List" ).render( {"items":data} )
                );
                loadTable();
            }
        );
        /**************************************/
        /* load table
        /*************************************/
        function loadTable() {
            $('#GenericTable').DataTable( {
                responsive: true
            } );

        }
        /**************************************/
        /* update   di base
        /*************************************/
        $.fn.save = function () {
            $.when(   $.module.savedata() )
                .then(
                    function( data ) {
                        console.log("savedata in clientjs", data);
                        $.module.update($rootingStore.getState().modulo, $rootingStore.getState().id, data);
                    }
                );
        }
        /**************************************/
        /* create   di base
        /*************************************/
        $.fn.create = function () {
            $.when(   $.module.savedata() )
                .then(
                    function( data ) {
                        console.log("createed in clientjs", data);
                        $.module.create($rootingStore.getState().modulo,  data);
                    }
                );
        }
    }); //document ready




});




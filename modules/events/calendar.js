/**
 * Created by paolo on 20/09/2017.
 */
$(function () {

    $(document).ready(function () {
        var events = [];
        $.module.loadMenu("default");
        if (localStorage.role!="admin") $("#utentiCal").hide();
        //definisco il menu
        loadCalendar();


        loadutenti();
        /*
         List of loadCalendar
         -----------------------------------------------------------*/
        function loadCalendar() {
            // var req = $.DataAccess.general("eventi", "list");
            // req.success(function (json) {

			setTimeout(function(){
                // var data = JSON.parse(json);
                // console.log ("eventi", data);
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next,today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay,listWeek'
                    },
                    lang: 'it',
                    timeFormat: 'H(:mm)',
                    editable: true,
                    navLinks: true, // can click day/week names to navigate views
                    eventLimit: true, // allow "more" link when too many events
                    eventSources: [
                        {
                            events: function (start, end, timezone, callback) {

                                if (localStorage.role == "admin") {
                                    var req = $.DataAccess.list("eventi", 'none');
                                    req.success(function (json) {

                                        // if (json.retVal === true) {
                                        var data = json;
                                        console.log ("eventi", data);
                                        events = [];
                                        for (var i = 0; i < data.length  ; i++) {

                                            events.push({
                                                id: data[i].id,
                                                title: data[i].title,
                                                start: moment(data[i].start).local(),
                                                end: moment(data[i].end).local(),
                                                backgroundColor: data[i].backgroundColor,
                                                textColor: data[i].textColor,
                                                color: data[i].color,
                                                name_operatore:data[i].name_operatore,
                                                generated_by:data[i].generated_by,
                                                note:data[i].note,
                                                allDay: false
                                            });
                                        }
                                        $("#calendar").fullCalendar("rerenderEvents");
                                        callback(events);

                                    })

                                } else if (localStorage.role == "consulente" || localStorage.role == "account" ) {
                                    var req = $.DataAccess.list("eventi", {"name_operatore": localStorage.username});
                                    req.success(function (json) {

                                        // if (json.retVal === true) {
                                        var data = json;
                                        console.log ("eventi", data);
                                        events = [];
                                        for (var i = 0; i < data.length  ; i++) {

                                            events.push({
                                                id: data[i].id,
                                                title: data[i].title,
                                                start: moment(data[i].start).local(),
                                                end: moment(data[i].end).local(),
                                                backgroundColor: data[i].backgroundColor,
                                                textColor: data[i].textColor,
                                                color: data[i].color,
                                                name_operatore:data[i].name_operatore,
                                                generated_by:data[i].generated_by,
                                                note:data[i].note,
                                                allDay: false
                                            });
                                        }
                                        $("#calendar").fullCalendar("rerenderEvents");
                                        callback(events);

                                    })


                                }


                            }
                        }
                    ],
                    loading: function(bool) {
                        $('#loading').toggle(bool);
                    },
                    drop: function(date) {

                        //console.log(date);
                        // is the "remove after drop" checkbox checked?
                        if ($('#drop-remove').is(':checked')) {
                            // if so, remove the element from the "Draggable Events" list
                            $(this).remove();
                        }
                    },
                    eventReceive: function (event){
                        //alert (event.title);		alert("Dropped on " + date.format());		alert (event.note);alert (event.start.format());
                        console.log ("evento creato",event);

                        //alert ($("#messages").attr("data-last"));
                    },
                    eventClick: function(event, element) {
                        console.log("EVENTO Click" , event);
                        localStorage.id = event.id;
                        localStorage.id_coniuge = event.generated_by.entity;
                        localStorage.inpage = "update";
                        // $.module.load('anagrafica/edit','eventi');
                        $('#calebndarBox').hide();
                        $('#eventiCreate').hide();
                        $('#eventiBox').show();

                        compileFormInPage(event);


                        //Gestione di associazione solo per admin 2 sporche call annidate a utenti e clienti
                        if (localStorage.role == "admin"){
                            $("#adminControlEdit").show();
                            var req2 = $.DataAccess.list("utenti", 'none');
                            req2.success(function (json) {
                                var utenti = json;
                                console.log("utenti in clienti", utenti);
                                //pusho nella select solo i consulenti
                                $.each(utenti, function(key, value) {
                                    if (value.role == "consulente" || value.role == "account") {
                                        $('.operatorAssoc')
                                            .append($("<option></option>")
                                                .attr("value",value.nome)
                                                .text(value.nome_utente));
                                    }
                                });
                                console.log ("mio evento operatore", event.name_operatore);
                                //read cliente cioccolato per impostare nome
                                $("#operatoreEdit").val(event.name_operatore);
                                var req3 = $.DataAccess.read("clienti",event.generated_by.entity);
                                req3.success(function (json) {
                                    console.log (json,"clienti_read");
                                    $("#id_cliente_name"). val (json.nome +" "+ json.cognome);

                                });
                                // compileForm(data);

                                //CLICK ON ASSIGN
                                $("#module").on('click', '#linkConiuge', function () {
                                    $('#generic-modal').modal('show');
                                    $("#InModal").empty();
                                    $(".modal-title").text("Assegna un cointestatario");
                                    //Se consulente vedo solo i miei
                                    var req2 = $.DataAccess.list("clienti", "none");
                                    req2.success(function (json) {
                                        var clientiC = json;
                                        console.log("clienti in clienti", clientiC);
                                        prova = {
                                            "clienti": clientiC,
                                        }
                                        $("#InModal").append(
                                            $("#clientitutto").render(prova)
                                        );


                                        setTimeout(function () {
                                            $('#GenericTable').DataTable({});
                                        }, 300);

                                        //Click nella modale di assegnazione cliente

                                        $("#module").on('click', '#unlinkConiuge', function (){
                                            $( "#id_coniuge" ).val("");
                                            $( "#name_Coniuge" ).val("");


                                        });
                                        $("#modals").on('click', '.pickConiuge', function (){
                                            $( "#id_coniuge" ).val($(this).attr("data-id"));
                                            $( "#name_Coniuge" ).val($(this).attr("data-name"));
                                            //$( "#usersHolder" ).fadeOut(500, function (){$( "#usersHolder" ).empty();});
                                            $('#generic-modal').modal('hide');
                                            $( "#linkConiuge" ).removeAttr("disabled");

                                        });
                                    });

                                });
                            });

                        }


                    },
                    eventDrop: function(event, delta, revertFunc) {
                        console.log("DROP", event);


                             if (event.title == "Cambio operatore") {
                                if (event.end==null){console.log("event end = event start" + event.end + event.start); event.end=event.start;}
                                   var savedata ={
                                        id: event.id,
                                        title: event.title,
                                       start: moment(event.start).local(),
                                       end: moment(event.end).local(),
                                        backgroundColor: event.backgroundColor,
                                        textColor: event.textColor,
                                        color: event.color,
                                        name_operatore:event.name_operatore,
                                        generated_by:event.generated_by,
                                        note:event.note,
                                        allDay: false
                                    };
                                var req = $.DataAccess.update(localStorage.modulo, event.id, savedata);
                                req.success(function (json) {
                                    $.toast({
                                        heading: "<h3 style='color:white; text-transform: uppercase'>"+localStorage.modulo+"</h3>",
                                        text: 'Dati salvati con successo',
                                        position: 'top-left',
                                        loaderBg:'#ff6849',
                                        icon: 'info',
                                        hideAfter: 3500,

                                        stack: 6
                                    })
                                    var data = json;
                                    console.log ("update data", data);


                                });
                             } else {
                                revertFunc();
                            }


                    },
                    eventResize: function(event, delta, revertFunc) {
                        console.log("DROP", event);
                        //console.log(event.id_pr);
                        if ( localStorage.role == "admin") {
                            if (event.end==null){console.log("event end = event start" + event.end + event.start); event.end=event.start;}
                            var savedata ={
                                id: event.id,
                                title: event.title,
                                start: moment(event.start).local(),
                                end: moment(event.end).local(),
                                backgroundColor: event.backgroundColor,
                                textColor: event.textColor,
                                color: event.color,
                                name_operatore:event.name_operatore,
                                generated_by:event.generated_by,
                                note:event.note,
                                allDay: false
                            };
                            // var req = $.DataAccess.general(localStorage.modulo, "update", event.id, savedata);
                            var req = $.DataAccess.update(localStorage.modulo, event.id, savedata);
                            req.success(function (json) {
                                $.toast({
                                    heading: "<h3 style='color:white; text-transform: uppercase'>"+localStorage.modulo+"</h3>",
                                    text: 'Dati salvati con successo',
                                    position: 'top-left',
                                    loaderBg:'#ff6849',
                                    icon: 'info',
                                    hideAfter: 3500,

                                    stack: 6
                                })
                                var data = json;
                                console.log ("update data", data);
                            });
                        } else {
                            revertFunc();
                        }
                    },
                    eventRender: function(event, element)
                    {
                        //alert(event.id);
                        //element.draggable();
                        //console.log("EVENT", event);
                        //element.addTouch();
                        // if (event.opzionato != "true") element.css(  "opacity" , 0.4 );
                        // if (event.opzionato == "true" && event.inviato != "true") element.css(  "opacity" , 1 );
                        // if (event.opzionato == "true" && event.inviato == "true" && event.confermato!="true") element.css(  "opacity" , 0.4 );
                        // if (event.confermato == "true") element.css(  "opacity" , 1 );

                        // if (event.confermato != "true" && event.opzionato != "true") element.find('.fc-content').css(  "opacity" , 0.5 );
                        // if (event.opzionato == "true" && event.inviato == "true") element.find('.fc-content').css(  "opacity" , 1 );
                        element.find('.fc-content').attr(  "id" , event.id );
                        element.find('.fc-time').html(  "<i style='width:10%' class='fa fa-clock-o'></i>" + event.start.format('HH:mm') +  "-" + event.end.format('HH:mm') );
                        // element.find('.fc-content').append("<div> <i style='width:10%' class='fa fa-map-marker'></i>" + event.location + "</div>" );
                        element.find('.fc-content').append("<div> <i style='width:10%' class='fa fa-male'></i> " + event.name_operatore + "</div>");
                    }

                });
			},100);
           // })

        } //fine calendar


        /**************************************/
        /* Update evento */
        /*************************************/
        $.fn.updEvent = function (id) {
            var savedata = {};
            var id_coniuge = $('#id_coniuge').val();
            if (id_coniuge == null || id_coniuge == "") {id_coniuge = localStorage.id_coniuge}
            savedata["generated_by"] = {
                "entity":id_coniuge,
                "entityType":"Cliente",
                "entityField":""
            };

            $('.takeIn').each(function () {
                var name = $(this).attr("name");
                var value = $(this).val();
                if ( (name == "start") || (name =="end") ){
                    var data = moment(value, "DD-MM-YYYY HH:mm");
                    console.log("START UPDATE", data);
                    value = data.format("YYYY-MM-DD HH:mm");
                }
                savedata[name] = value;
            });
            console.log("DATI SALVATI", savedata);
			
			$.when(  $.module.validateRequired() ).then(function( errorObj ) {
				console.log ("after validation counter", errorObj.counter);
				if (errorObj.counter == 0){
						
					var req = $.DataAccess.update(localStorage.modulo, id, savedata);
					req.success(function (json) {
						$.toast({
							heading: "<h3 style='color:white; text-transform: uppercase'>"+localStorage.modulo+"</h3>",
							text: 'Dati salvati con successo',
							position: 'top-left',
							loaderBg:'#ff6849',
							icon: 'info',
							hideAfter: 3500,

							stack: 6
						})
						var data = json;
						console.log ("update data", data);

						if (localStorage.position == "calendar") {
							$('#calendar').fullCalendar('removeEvents');
							if (localStorage.role != "admin") {
                                var req2 = $.DataAccess.list("eventi", {"name_operatore": localStorage.username});
                            } else {
                                var req2 = $.DataAccess.list("eventi", "none");
                            }
							req2.success(function (json) {
								var data2 = json;
                                $('#calendar').fullCalendar('addEventSource', data2);
								// $('#calendar').fullCalendar( 'refetchEvents', data );
								$('#calebndarBox').show();
								$('#eventiBox').hide();
							});

						}

					});
				}
				else
				{

					var html='<p>Per salvare tutti i seguenti campi devono essere compilati:</p>';
					for (i = 0; i < errorObj.errors.length; i++) {
					   html += '<p>' + errorObj.errors[i] + '</p>';

					}
					$.toast({
						heading: "ATTENZIONE",
						text:  html,
						position: 'top-left',
						loaderBg:'#fb9678',
						icon: 'warning',
						hideAfter: 5000,

						stack: 6
					});
					// $("#module [name|='"+name+"']").css("border-color","red");
				}	
			})
        };
        /*******DISPLAY*******************/
        $("#module").on("click", "#addBtn", function () {
            localStorage.inpage = "create";
            $('#calebndarBox').hide();
            $('#eventiCreate').show();
            $('#eventiBox').hide();
            setpicker();
            //Gestione di associazione solo per admin 2 sporche call annidate a utenti e clienti
            if (localStorage.role == "admin"){
                $("#adminControl").show();
                var req2 = $.DataAccess.list("utenti", 'none');
                req2.success(function (json) {
                    var utenti = json;
                    console.log("utenti in clienti", utenti);
                    //pusho nella select solo i consulenti
                    $.each(utenti, function(key, value) {
                        if (value.role == "consulente" || value.role == "account") {
                            $('.operatorAssoc')
                                .append($("<option></option>")
                                    .attr("value",value.nome)
                                    .text(value.nome_utente));
                        }
                    });

                    // compileForm(data);
                    $("#module").on('click', '#linkConiuge1', function () {
                        $('#generic-modal').modal('show');
                        $("#InModal").empty();
                        $(".modal-title").text("Assegna un cointestatario");
                        //Se consulente vedo solo i miei



                        var req2 = $.DataAccess.list("clienti", "none");
                        req2.success(function (json) {
                            var clientiC = json;
                            console.log("clienti in clienti", clientiC);
                            prova = {
                                "clienti": clientiC,
                            }
                            $("#InModal").append(
                                $("#clientitutto").render(prova)
                            );


                            setTimeout(function () {
                                $('#GenericTable').DataTable({});
                            }, 300);

                            //Click nella modale di assegnazione cliente

                            $("#module").on('click', '#unlinkConiuge1', function (){
                                $( "#id_coniuge" ).val("");
                                $( "#name_Coniuge" ).val("");


                            });
                            $("#modals").on('click', '.pickConiuge', function (){
                                console.log ($(this).attr("data-id"), $(this).attr("data-name"))

                                $( "#id_coniuge1" ).val($(this).attr("data-id"));
                                $( "#id_cliente1" ).val($(this).attr("data-name"));
                                //$( "#usersHolder" ).fadeOut(500, function (){$( "#usersHolder" ).empty();});
                                $('#generic-modal').modal('hide');
                                $( "#linkConiuge" ).removeAttr("disabled");

                            });
                        });

                    });
                });

            }
        });

        /*******AGGIUNGO UN EVENTO*******************/
        $.fn.addEvent = function () {
            var adddata = {};
            var id_coniuge = $('#id_coniuge1').val();
            if (id_coniuge ) {
                adddata["generated_by"] = {
                    "entity":id_coniuge,
                    "entityType":"Cliente",
                    "entityField":""
                };

            }

            $('.create .takeIn').each(function () {
                var name = $(this).attr("name");
                var value = $(this).val();
                //console.log("NAME", name, "-", value);
                if ( (name == "start") || (name =="end") ){
                    // var data = moment(value, "DD-MM-YYYY hh:mm");
					value = moment(value, "DD-MM-YYYY HH:mm").local().toDate();
                }
                adddata[name] = value;
            });
            if (localStorage.role == "consulente" || localStorage.role == "account" ){
                adddata["name_operatore"] = localStorage.username;
            }

            $.when(  $.module.validateRequired() ).then(function( errorObj ) {
                console.log ("after validation counter", errorObj.counter);
				if (errorObj.counter == 0){
						
					 console.log("ADD  CALENDAR", adddata);
            //var req = $.DataAccess.general("eventi", "create", "", adddata);
					var req = $.DataAccess.create("eventi",  adddata);
					req.success(function (json) {
						$.toast({
							heading: "<h3 style='color:white; text-transform: uppercase'>"+localStorage.modulo+"</h3>",
							text: 'Evento creato con successo',
							position: 'top-left',
							loaderBg:'#ff6849',
							icon: 'info',
							hideAfter: 3500,

							stack: 6
						})
						var data = json;
						console.log ("add data", data);
						$('#calendar').fullCalendar('removeEvents');
                        if (localStorage.role != "admin") {
                            var req2 = $.DataAccess.list("eventi", {"name_operatore": localStorage.username});
                        } else {
                            var req2 = $.DataAccess.list("eventi", "none");
                        }
						req2.success(function (json) {
							var data2 = json;
							console.log ("list2", data2);
							//$('#calendar').fullCalendar({ events: data2});
							//$('#calendar').fullCalendar('renderEvent', data2, true);
							$('#calendar').fullCalendar('addEventSource', data2);
							$('#calebndarBox').show();
							$('#eventiBox').hide();
							$('#eventiCreate').hide();
						});


					});
				}
				else
				{

					var html='<p>Per creare tutti i seguenti campi devono essere compilati:</p>';
					for (i = 0; i < errorObj.errors.length; i++) {
					   html += '<p>' + errorObj.errors[i] + '</p>';

					}
					$.toast({
						heading: "ATTENZIONE",
						text:  html,
						position: 'top-left',
						loaderBg:'#fb9678',
						icon: 'warning',
						hideAfter: 5000,

						stack: 6
					});
					// $("#module [name|='"+name+"']").css("border-color","red");
				}	
			})
			
			
           
        };
        /**************************/

        /*******SETTO I DATETIME PICKER*******************/
        function setpicker() {
            var checkin1 = $('.start').datetimepicker({

                format: 'dd-mm-yyyy hh:ii',
                todayBtn: true,

                autoclose: true

            }).on('changeDate', function (ev) {
                console.log("ev", ev);


                var newDate = new Date(ev.date);
				
                //newDate.setDate(newDate.getDate() + 1 );  mette un giorno in piu del piker 1
                newDate.setDate(newDate.getDate() );   //mette la stessa data del picker 1
				// newDate.setHours(newDate.getHours() - 1);
                console.log("newDate", newDate);
                checkout1.datetimepicker("update", newDate);


                $('.end')[0].focus();
            });


            var checkout1 = $('.end').datetimepicker({
                beforeShowDay: function (date) {
                    console.log("date", date);
                    if (!checkin1.datetimepicker("getDate").valueOf()) {
                        return date.valueOf() >= new Date().valueOf();
                    } else {
                        return date.valueOf() > checkin1.datetimepicker("getDate").valueOf();
                    }
                },
                format: 'dd-mm-yyyy hh:ii',
                todayBtn: true,

                autoclose: true

            }).on('changeDate', function (ev) {});



        }



        /*******COMPILO FOM IN PAGE*******************/
       function compileFormInPage(data) {
            $(".update input").each(function( replace ) {
                var pointer = $(this).attr("name");
                console.log("pointer", pointer  );
                if ( (pointer == "start") || (pointer =="end") ){
                    var date =   moment(data[pointer]).local().format("DD-MM-YYYY HH:mm");
                    // var data = moment(value, "YYYY-MM-DD hh:mm ");
                    //value = data;
                    console.log("date in Compile", date);
                    data[pointer] = date;
                }

                $(this).val(data[pointer]);
            });
            $(".update textarea").each(function( replace ) {
                var pointer = $(this).attr("id");
                // if (typeof data[pointer] === 'string' ){
                //     var up = data[pointer].toUpperCase();
                //     $(this).val(up);
                // }else {
                //     $(this).val(data[pointer]);
                // }

                $(this).val(data[pointer]);
            });
            setpicker();
        }

        //FILTRO STATO SU SERVIZI
        $('#module').on("change", "#utentiCal", function() {
            var valueSelected = this.value;
            if (valueSelected != "filtra") {
                var req2 = $.DataAccess.list("eventi", {"name_operatore": valueSelected});
            } else {
                var req2 = $.DataAccess.list("eventi", "none");
            }

                req2.success(function (json) {
                var data2 = json;
                    console.log("data2", data2);
                    $('#calendar').fullCalendar('removeEvents');
                    $('#calendar').fullCalendar('addEventSource', data2);
                    $("#calendar").fullCalendar("rerenderEvents");
                 });


        });

       function  loadutenti() {
           var req5 = $.DataAccess.list("utenti", "none");
           req5.success(function (json) {
               var utenti = json;
               console.log("utenti", utenti);
               $.each(utenti, function(key, value) {
                   if (value.role == "consulente" || value.role == "account") {
                       $('#utentiCal')
                           .append($("<option></option>")
                               .attr("value",value.nome)
                               .text(value.nome_utente));
                   }


               });
           });

       }


    }); //document ready

});

function addEvent() {
    $.fn.addEvent();
}

function updEvent(id) {
    $.fn.updEvent(id);
}


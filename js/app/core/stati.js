(function ($) {
    $.stati = (function (my) {
        //installationg nutti 159
        var statiLead = [];
        var statiServizio = [];
        var avServizio = [];
        var regioni = [];
        var tipifile = [];
        var servizi = {
            entita : 'servizio',
            url : 'servizi'
        };
        var eventi = {
            entita : 'Evento',
            url : 'eventi'
        };
        var notifications = {
            entita : 'Notifica',
            url : 'notifications'
        };
        var clienti = {
            entita : 'Cliente',
            url : 'clienti'
        };




        //STATI LEAD
        //=========================================================
        statiLead[0] = 'NON ASSEGNATO';
        statiLead[1] = 'CONTATTO EFFETTUATO';
        statiLead[2] = 'DA RICONTATTARE';
        statiLead[3] = 'IN LAVORAZIONE';
        // statiLead[4] = 'PROCESSATO';
        statiLead[5] = 'CLIENTE';
        // statiLead[6] = 'GIÀ PRESENTE';
        statiLead[7] = 'NEGATIVO';
        statiLead[8] = 'ATTESA RISPOSTA PREVENTIVO';
        statiLead[9] = 'PREVENTIVO DA INVIARE';
        //=========================================================

        //STATI SERVIZI
        //=========================================================

        statiServizio[0] = 'DA PREVENTIVARE';
        statiServizio[1] = 'IN ATTESA DI CONFERMA';
        statiServizio[2] = 'ATTESA ACCONTO';
        statiServizio[3] = 'ATTESA SALDO';
        statiServizio[4] = 'IN CORSO';
        statiServizio[5] = 'TERMINATO';
        // statiServizio[6] = 'vuota';
        // statiServizio[7] = 'vuota';
        // statiServizio[8] = 'vuota';
        // statiServizio[9] = 'vuota';
        //=========================================================

        //AVANZAMENTO SERVIZI
        //=========================================================


        avServizio[0] = 'TERMINATO';
        avServizio[1] = 'IN CORSO';
        avServizio[2] = 'IN ATTESA DI DOCUMENTI';
        avServizio[3] = 'DA FATTURARE';
        avServizio[4] = 'CONGELATO';
        // avServizio[5] = 'TERMINATO';
        // statiServizio[6] = 'vuota';
        // statiServizio[7] = 'vuota';
        // statiServizio[8] = 'vuota';
        // statiServizio[9] = 'vuota';

        //TIPI FILE
        //=========================================================

        tipifile[0] = 'CONTRATTO';
        tipifile[1] = 'PREVENTIVO';
        tipifile[2] = 'NDA';
        tipifile[3] = 'BRIEF';
        tipifile[4] = 'PROPOSTA GRAFICA';
        tipifile[5] = 'SCHEMA DI REALIZZAZIONE';
        tipifile[6] = 'CESSIONE CODICE SORGENTE';
        // tipifile[7] = 'vuota';
        // tipifile[8] = 'vuota';
        // tipifile[9] = 'vuota';
        //=========================================================


        //REGIONI
        //=========================================================

        regioni[0] = 'ABRUZZO';
        regioni[1] = 'BASILICATA';
        regioni[2] = 'CALABRIA';
        regioni[3] = 'CAMPANIA';
        regioni[4] = 'EMILIA-ROMAGNA';
        regioni[5] = 'FRIULI-VENEZIA-GIULIA';
        regioni[6] = 'LAZIO';
        regioni[7] = 'LIGURIA';
        regioni[8] = 'LOMBARDIA';
        regioni[9] = 'MARCHE';
        regioni[10] = 'MOLISE';
        regioni[11] = 'PIEMONTE';
        regioni[12] = 'PUGLIA';
        regioni[13] = 'SARDEGNA';
        regioni[14] = 'SICILIA';
        regioni[15] = 'TOSCANA';
        regioni[16] = 'TRENTINO-ALTO ADIGE';
        regioni[17] = 'UMBRIA';
        regioni[18] = 'VALLE D AOSTA';
        regioni[19] = 'VENETO';

        //=========================================================



//getter degli stati
        my.get = function () {
            var resources = [];
            //varibili di stati
            resources['statiLead'] = statiLead;
            resources['statiServizio'] = statiServizio;
            resources['avServizio'] = avServizio;
            resources['regioni'] = regioni;
            resources['tipifile'] = tipifile;
            console.log("stati", resources );
            return resources;
        };

        my.getModule = function () {
            var resources = {};
            resources.servizi = servizi;
            resources.eventi = eventi;
            resources.notifications = notifications;
            resources.clienti = clienti;
            return resources;
        };


        return my;
    })({})

})(jQuery);


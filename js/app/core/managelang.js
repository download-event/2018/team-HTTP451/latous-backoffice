﻿$(function () {
    $(document).ready(function () {
       var $langResources = new Array();
        ImpostLang();
        //langEn
        $('.langEn').on('click', function (e) {
            //alert("En");
            localStorage.setItem("CurrentLanguage", "en");
            //$langResources = getLanguageResources()["en"];
            $langResources = $.languages.get()["en"];
            setlanguage();
            //moment.locale("en");
        });

        //langIt
        $('.langIt').on('click', function (e) {
            //alert("It");
            localStorage.setItem("CurrentLanguage", "it");
            //$langResources = getLanguageResources()["it"];
            $langResources = $.languages.get()["it"];
            setlanguage();
            //moment.locale("it");
        });
    });

    // Update 1.6
//SET LANGUAGES
    function setlanguage() {
        //console.log("dentro setlanguage");
        $("span[name='lbl']").each(function (i, elt) {
            //console.log("DEBUG==>elt", elt);
            try {
                //console.log("try", $langResources[$(elt).attr("caption")]);
                $(elt).text($langResources[$(elt).attr("caption")]);
                if ($(elt).parent().attr('for')) {
                    var xxx = $(elt).parent().attr('for');
                    $('#' + xxx).attr('placeholder', $(elt).text());
                    //console.log(xxx);
                }

            }
            catch (err) {
                console.log("err");
                console.log(err);
            }
        });

        //moment.locale(localStorage.getItem("CurrentLanguage"));
    }

    //var $langResources = new Array();
    function ImpostLang() {
        console.log("dento IMPOST LANG");
        //moment.locale("it");
        if (!localStorage.getItem('CurrentLanguage')) {
            localStorage.setItem("CurrentLanguage", "en");
            $langResources = $.languages.get()["en"];
            //moment.locale("en");
        }
        else {
            $langResources = $.languages.get()[localStorage.getItem("CurrentLanguage")];
            //moment.locale(localStorage.getItem("CurrentLanguage"));
        }

        setlanguage();
    }
});
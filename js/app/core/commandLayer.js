(function ($) {
    $.commandLayer = (function (my) {
        //Device Commands 
        //========================================================================================
		 my.commands = function (command, deviceType, installationId, deviceCode, msg ) {
             //console.log("comando reale inviato", '/'+deviceType+'/'+command+'/138/'+deviceCode);
             //return operation_post_command('/'+deviceType+'/'+command+'/'+installationId+'/' + deviceCode, JSON.stringify(msg));
             return operation_post_command('/'+deviceType+'/'+command+'/'+installationId, JSON.stringify(msg));
            //return operation_post_command('/'+deviceType+'/'+command+'/80/'+deviceCode, JSON.stringify({}));
		 }

		 my.wagoCommand = function (endpoint, data) {		    
		     return operation_post_command(endpoint, data);
		 }

        //DEVICE
        //========================================================================================
        my.send_modbus_to_plc = function (installationId, DeviceType,  msg) {
            //DeviceType = DeviceType.toLowerCase();
		     console.log("installationId", installationId, "DeviceType", DeviceType)
            return operation_post_command('/'+DeviceType+'/setmodbus/' + installationId, JSON.stringify(msg));
        }

        my.send_modbus_to_plc_pool = function (installationId, DeviceCode,  msg) {

            return operation_post_command('/pool/setmodbus/' + installationId + '/' + DeviceCode, JSON.stringify(msg));
        }
        //========================================================================================

        //METER2
        //========================================================================================
        my.send_modbus_to_meter = function (msg) {
            return operation_post_command('http://54.229.200.152/client-stream' , JSON.stringify(msg));
        }
        //========================================================================================

        //REQUEST LOG
        //========================================================================================
        my.requestlog = function (installationId, msg) {
            return operation_post_command('/installations/requestlog/'+ installationId  , JSON.stringify(msg));
        }
        //========================================================================================//

        // CRONOTERMOSTATI COMANDI DI SETTINGS
        //========================================================================================
        my.tasklist = function (installationId, msg) {
            return operation_post_command('/crontasks/list/'+ installationId  , JSON.stringify(msg));
        }
        my.tasklist_current = function (installationId, msg) {
            return operation_post_command('/crontasks/getCurrent/'+ installationId  , JSON.stringify(msg));
        }
        my.updatetask = function (installationId, msg) {
            return operation_post_command('/crontasks/update/'+ installationId  , JSON.stringify(msg));
        }
        my.createtask = function (installationId, msg) {
            return operation_post_command('/crontasks/add/'+ installationId  , JSON.stringify(msg));
        }
        my.deletetask = function (installationId, msg) {
            return operation_post_command('/crontasks/del/'+ installationId  , JSON.stringify(msg));
        }
        my.getcronprofile = function (installationId, msg) {
            return operation_post_command('/cron/getprofile/'+ installationId  , JSON.stringify(msg));
        }
        my.restartcron = function (installationId, msg) {
            return operation_post_command('/cron/restart/'+ installationId  , JSON.stringify(msg));
        }

        my.savecronprofile = function (installationId, msg) {
            console.log("save MSg", msg);
            return operation_post_command('/cron/saveprofile/'+ installationId  , JSON.stringify(msg));
        }
        my.setcronprofile = function (installationId, msg) {
            console.log("set MSg", msg);
            return operation_post_command('/cron/setprofile/'+ installationId  , JSON.stringify(msg));
        }
        my.getcalendarMonthCron = function (installationId, msg) {
            return operation_post_command('/cron/getcalendarmonth/'+ installationId  , JSON.stringify(msg));
        }
        my.setcalendarMonthCron = function (installationId, msg) {
            return operation_post_command('/cron/setcalendarmonth/'+ installationId  , JSON.stringify(msg));
        }
        my.savecalendarMonthCron = function (installationId, msg) {
            return operation_post_command('/cron/savecalendarmonth/'+ installationId  , JSON.stringify(msg));
        }
        my.replaceDesiredWithTask = function (installationId, msg) {
            return operation_post_command('/cron/replaceDesiredWithTask/'+ installationId  , JSON.stringify(msg));
        }

        //========================================================================================
        // CAL / GRU / VRD COMANDI DI SETTINGS
        //========================================================================================
        my.readClimatic = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/getclimatic/'+ installationId  , JSON.stringify(msg));
        }
		my.getClimaticList = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/listcc/'+ installationId  , JSON.stringify(msg));
        }
        my.addClimatic = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/addcc/'+ installationId  , JSON.stringify(msg));
        }
        my.setClimatic = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/setclimatic/'+ installationId  , JSON.stringify(msg));
        }
        my.updClimatic = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/updcc/'+ installationId  , JSON.stringify(msg));
        }
        my.delClimatic = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/delcc/'+ installationId  , JSON.stringify(msg));
        }
        my.tryrearm = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/tryrearm/'+ installationId  , JSON.stringify(msg));
        }
        my.setdelay = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/setdelay/'+ installationId  , JSON.stringify(msg));
        }
        my.getdelay = function (installationId, type, msg) {
			return operation_post_command('/'+type+'/getdelay/'+ installationId  , JSON.stringify(msg));
        }
		my.getbackup = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/getBackupWorkingMode/'+ installationId  , JSON.stringify(msg));
        }
		my.gethysteresis = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/gethysteresis/'+ installationId  , JSON.stringify(msg));
        }
		 my.sethysteresis = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/sethysteresis/'+ installationId  , JSON.stringify(msg));
        }
		my.setpulse = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/setpulsecounter/'+ installationId  , JSON.stringify(msg));
        }
		my.getcommandvalue = function (installationId, type, msg) {
			return operation_post_command('/'+type+'/getCommands/'+ installationId  , JSON.stringify(msg));
        }
		my.setcommandvalue = function (installationId, type, msg) {
			return operation_post_command('/'+type+'/setCommands/'+ installationId  , JSON.stringify(msg));
        }
		my.getflow = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/getflowtemperaturelimits/'+ installationId  , JSON.stringify(msg));
        }
		my.setflow = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/setflowtemperaturelimits/'+ installationId  , JSON.stringify(msg));
        }
		my.getpulse = function (installationId, type, msg) {
            return operation_post_command('/'+type+'/getpulsecounter/'+ installationId  , JSON.stringify(msg));
        }
		
        //========================================================================================
        // SCA COMANDI DI SETTINGS
        //========================================================================================
        my.getScaStatus = function (installationId, msg) {
            return operation_post_command('sca/getscastate/'+ installationId  , JSON.stringify(msg));
        }

        //========================================================================================
        // HVAC COMANDI DI SETTINGS
        //========================================================================================
        my.setpointcoldheat = function (installationId, msg) {
            return operation_post_command('/hvac/setpointcoldheat/'+ installationId  , JSON.stringify(msg));
        }
        my.setfreeCooling = function (installationId, msg) {
            return operation_post_command('/hvac/setfreeCooling/'+ installationId  , JSON.stringify(msg));
        }
        my.getsupplyFan = function (installationId, msg) {
            return operation_post_command('/hvac/getsupplyFan/'+ installationId  , JSON.stringify(msg));
        }
        my.setppmShuttermin = function (installationId, msg) {
            return operation_post_command('/hvac/setppmShuttermin/'+ installationId  , JSON.stringify(msg));
        }
        my.requestParam = function (installationId, msg) {
            return operation_post_command('/hvac/requestParam/'+ installationId  , JSON.stringify(msg));
        }
        my.setsupplyFan = function (installationId, msg) {
            return operation_post_command('/hvac/setsupplyFan/'+ installationId  , JSON.stringify(msg));
        }
		 //========================================================================================
        // EOVARS
        //========================================================================================
        my.pippo2 = function (installationId, msg) {
		     console.log("getovar", msg);
             //return operation_post_command2('/cron/getprofile/'+ '16'  , JSON.stringify(msg));
             return operation_post_command2('/eovar/getvar/'+ installationId  , JSON.stringify(msg))
        }

		
		my.getEovars = function (installationId, type, msg) {
			var output = [] ;
			$.each (msg, function (key, value){
				// console.log ("valueInCommandlayer", key, value )
				// var splitted = value.code.split('_');
				var temp = operation_post_command('/eovar/getvar/'+ installationId  , JSON.stringify(value))
				// console.log ("temp", temp.responseJSON.retval )
				if (temp.responseJSON.retval) output.push (temp.responseJSON.dataObject);
				// console.log ("eovarselected", $eovars["eovar0"+ splitted[1]] )
				// output.push ($eovars[value.code]);
			});
			
			console.log ("valueInCommandlayerOUTPUT", output )
			return output
		}
		my.setEovars = function (installationId, msg) {
			return operation_post_command('/eovar/setVar/'+ installationId  , JSON.stringify(msg))
		}




        function noConnection() {
            $.toast({
                heading: 'errore di comando al Plc',
                text: 'Aspettare 5 minuti e riprovare',
                position: 'top-left',
                loaderBg:'#ff6849',
                icon: 'info',
                hideAfter: 3500,

                stack: 6
            })
        }

        function operation_get (endpoint, data) {
            var urlBase = $.appParms.ulrCommandLayer();
            return $.ajax({
                datatype: 'json',
                url: urlBase + endpoint,
                headers: {
                    "x-access-token": localStorage.token
                },
                data: data,
                type: 'GET',
                beforeSend: function () {},
                complete: function () {},
                error: function (xhr, status) {
                    noConnection();
                }
            }); //ajax
        }



        function operation_post_command (endpoint, data) {
            var urlBase = $.appParms.ulrCommandLayer();
            return $.ajax({
                contentType: "application/json; charset=utf-8",
                datatype: 'json',
                url: urlBase + endpoint,
                // url: endpoint,
                headers: {
                    "x-access-token": localStorage.token
                },
                data: data,
                type: 'POST',
                cache: false,
                processData: false,
                async: false,
                beforeSend: function () {},
                complete: function () {},
                error: function (xhr, status) {
                    noConnection();
                }
            }); //ajax
        }

        function operation_post_command2 (endpoint, data) {
            var urlBase = $.appParms.ulrCommandLayer();
            return $.ajax({
                contentType: "application/json; charset=utf-8",
                datatype: 'json',
                url: urlBase + endpoint,
                // url: endpoint,
                headers: {
                    "x-access-token": localStorage.token
                },
                data: data,
                type: 'POST',
                cache: false,
                processData: false,
                async: true,
                beforeSend: function () {},
                done: function () {},
                error: function (xhr, status) {
                    noConnection();
                }
            }); //ajax
        }


        return my;

    })({});
})(jQuery)
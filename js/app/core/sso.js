(function ($) {
    $.sso = (function (my) {
        //Login
        //========================================================================================
        my.login = function (msg) {
            return operation_post_login('/login', JSON.stringify(msg));
        }
        my.gwtTransale = function () {
            return operation_get('/checkJWT'  , JSON.stringify({}));
        };


        //Utenti
        //========================================================================================
        my.utenti_list = function () {
            return operation_get('/users', JSON.stringify({}));
        }
		 my.utenti_get = function (username) {
            return operation_get('/user/'+username, JSON.stringify({}));
        }
		my.utenti_enable = function (username) {
            return operation_post('/user/'+username+'/enable', JSON.stringify({}));
        }
		my.utenti_disable = function (username) {
            return operation_post('/user/'+username+'/disable', JSON.stringify({}));
        }
		my.utenti_create = function (msg) {
            return operation_post('/user', JSON.stringify(msg));
        }
		my.utenti_delete = function (username) {
            return operation_delete('/user/'+username, JSON.stringify(msg));
        }
        my.resetPwd = function (msg, username) {
            return operation_post('/user/'+username+'/adminreset', JSON.stringify(msg));
        }

        my.changeRole = function (msg, username) {
            return operation_put('/user/'+username+'/roles', JSON.stringify(msg));
        }
		my.changeProject = function (msg, username) {
            return operation_put('/user/'+username+'/projects', JSON.stringify(msg));
        }
		
		
        function noConnection() {
            $.toast({
                heading: 'errore di login',
                text: 'contattare clevergy',
                position: 'top-left',
                loaderBg:'#ff6849',
                icon: 'info',
                hideAfter: 3500,

                stack: 6
            })
        }

        function operation_delete (endpoint, data) {
            var urlBase = $.appParms.ulrSso();
            return $.ajax({
                contentType: "application/json; charset=utf-8",
                datatype: 'json',
                url: urlBase + endpoint,
				headers: {
                    "x-access-token": localStorage.token
                },
                data: data,
                type: 'DELETE',
                async: false,
                beforeSend: function () {},
                complete: function () {},
                error: function (xhr, status) {
                    noConnection();
                }
            }); //ajax
        }

        function operation_get (endpoint, data) {
            var urlBase = $.appParms.ulrSso();
            return $.ajax({
                datatype: 'json',
                url: urlBase + endpoint,
                headers: {
                    "x-access-token": localStorage.token
                },
                data: data,
                type: 'GET',
                beforeSend: function () {},
                complete: function () {},
                error: function (xhr, status) {
                    noConnection();
                }
            }); //ajax
        }


        function operation_post_login (endpoint, data) {
            var urlBase = $.appParms.ulrSso();
            return $.ajax({
                contentType: "application/json; charset=utf-8",
                datatype: 'json',
                url: urlBase + endpoint,
                data: data,
                type: 'POST',
                cache: false,
                processData: false,
                async: false,
                beforeSend: function () {},
                complete: function () {},
                error: function (xhr, status) {
                    noConnection();
                }
            }); //ajax
        }

        function operation_post (endpoint, data) {
            var urlBase = $.appParms.ulrSso();
            return $.ajax({
                contentType: "application/json; charset=utf-8",
                datatype: 'json',
                url: urlBase + endpoint,
                headers: {
                    "x-access-token": localStorage.token
                },
                data: data,
                type: 'POST',
                cache: false,
                processData: false,
                async: false,
                beforeSend: function () {},
                complete: function () {},
                error: function (xhr, status) {
                    noConnection();
                }
            }); //ajax
        }

        function operation_put (endpoint, data) {
            var urlBase = $.appParms.ulrSso();
            return $.ajax({
                contentType: "application/json; charset=utf-8",
                datatype: 'json',
                url: urlBase + endpoint,
				headers: {
                    "x-access-token": localStorage.token
                },
                data: data,
                type: 'PUT',
                cache: false,
                processData: false,
                async: false,
                beforeSend: function () {},
                complete: function () {},
                error: function (xhr, status) {
                    noConnection();
                }
            }); //ajax
        }

        return my;

    })({});
})(jQuery)
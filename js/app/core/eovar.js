﻿(function ($) {
    $.eovar = (function (my) {
        //installationg nutti 159
        var eovar1_159 = [];
        var eovar2_159 = [];
        var eovar3_159 = [];
        var eovar4_159 = [];
        var eovar5_159 = [];
        var eovar6_159 = [];
        var eovar7_159 = [];
        //installation gnutti 221
        var eovar1_221 = [];
        var eovar2_221 = [];
        var eovar3_221 = [];



         //EOVAR1
         //=========================================================
        eovar1_159[0] = 'SelManAut';
        eovar1_159[1] = 'SelC2_A_B';
        eovar1_159[2] = 'SelSetEqual';
        eovar1_159[3] = 'TGP_utente';
        eovar1_159[4] = 'TGP_min';
        eovar1_159[5] = 'TGP_max';
        eovar1_159[6] = 'DEADZONE';
        eovar1_159[7] = 'vuota';
        eovar1_159[8] = 'vuota';
        eovar1_159[9] = 'vuota';
        //=========================================================

        //EOVAR2
        //=========================================================
        eovar2_159[0] = 'TGP_203';
        eovar2_159[1] = 'TGP_303';
        eovar2_159[2] = 'TGP_304';
        eovar2_159[3] = 'TGP_305';
        eovar2_159[4] = 'TGP_403';
        eovar2_159[5] = 'TGP_402';
        eovar2_159[6] = 'vuota';
        eovar2_159[7] = 'vuota';
        eovar2_159[8] = 'vuota';
        eovar2_159[9] = 'vuota';
        //=========================================================

        //EOVAR3
        //=========================================================
        eovar3_159[0] = 'C2C_C101';
        eovar3_159[1] = 'C2C_C102';
        eovar3_159[2] = 'C2C_C103';
        eovar3_159[3] = 'C2C_C104';
        eovar3_159[4] = 'C2C_C105';
        eovar3_159[5] = 'C101_DischargePressure';
        eovar3_159[6] = 'C102_DischargePressure';
        eovar3_159[7] = 'C103_DischargePressure';
        eovar3_159[8] = 'C104_DischargePressure';
        eovar3_159[9] = 'C105_DischargePressure';
        //=========================================================

        //EOVAR4
        //=========================================================
        eovar4_159[0] = 'DP_TGP_C101';
        eovar4_159[1] = 'DP_ASP_C101';
        eovar4_159[2] = 'DP_OFP_C102';
        eovar4_159[3] = 'DP_ONP_C102';
        eovar4_159[4] = 'DP_OFP_C103';
        eovar4_159[5] = 'DP_ONP_C103';
        eovar4_159[6] = 'DP_OFP_C104';
        eovar4_159[7] = 'DP_ONP_C104';
        eovar4_159[8] = 'DP_TGP_C105';
        eovar4_159[9] = 'DP_ASP_C105';
        //=========================================================

        //EOVAR5
        //=========================================================
        eovar5_159[0] = 'PT_101';
        eovar5_159[1] = 'DP_C2A';
        eovar5_159[2] = 'DP_C2B';
        eovar5_159[3] = 'PT_203';
        eovar5_159[4] = 'PT_303';
        eovar5_159[5] = 'PT_304';
        eovar5_159[6] = 'PT_305';
        eovar5_159[7] = 'PT_403';
        eovar5_159[8] = 'PT_402';
        eovar5_159[9] = 'vuota';
        //=========================================================

        //EOVAR6
        //=========================================================
        eovar6_159[0] = 'TGP_C101';
        eovar6_159[1] = 'ASP_C101';
        eovar6_159[2] = 'ONP_C102';
        eovar6_159[3] = 'OFP_C102';
        eovar6_159[4] = 'ONP_C103';
        eovar6_159[5] = 'OFP_C103';
        eovar6_159[6] = 'ONP_C104';
        eovar6_159[7] = 'OFP_C104';
        eovar6_159[8] = 'TGP_C105';
        eovar6_159[9] = 'ASP_C105';
        //=========================================================

        //EOVAR7
        //=========================================================
        eovar7_159[0] = 'R_TGP_C101';
        eovar7_159[1] = 'R_ASP_C101';
        eovar7_159[2] = 'R_ONP_C102';
        eovar7_159[3] = 'R_OFP_C102';
        eovar7_159[4] = 'R_ONP_C103';
        eovar7_159[5] = 'R_OFP_C103';
        eovar7_159[6] = 'R_ONP_C104';
        eovar7_159[7] = 'R_OFP_C104';
        eovar7_159[8] = 'R_TGP_C105';
        eovar7_159[9] = 'R_ASP_C105';
        //=========================================================


        //GNUTTI 221 EOVAR
        //EOVAR1
        //=========================================================
        eovar1_221[0] = 'soglia_LUX01';
        eovar1_221[1] = 'isteresi_LUX01';
        eovar1_221[2] = 'soglia_LUX02';
        eovar1_221[3] = 'isteresi_LUX02';
        eovar1_221[4] = 'soglia_LUX03';
        eovar1_221[5] = 'isteresi_LUX03';
        eovar1_221[6] = 'soglia_LUX04';
        eovar1_221[7] = 'isteresi_LUX04';
        eovar1_221[8] = 'soglia_LUX05';
        eovar1_221[9] = 'isteresi_LUX05';
        //=========================================================

        //EOVAR2
        //=========================================================
        eovar2_221[0] = 'soglia_LUX06';
        eovar2_221[1] = 'isteresi_LUX06';
        eovar2_221[2] = 'soglia_LUX07';
        eovar2_221[3] = 'isteresi_LUX07';
        eovar2_221[4] = 'soglia_LUX08';
        eovar2_221[5] = 'isteresi_LUX08';
        eovar2_221[6] = 'soglia_LUX09';
        eovar2_221[7] = 'isteresi_LUX09';
        eovar2_221[8] = 'soglia_LUX10';
        eovar2_221[9] = 'isteresi_LUX10';
        //=========================================================

        //EOVAR3
        //=========================================================
        eovar3_221[0] = 'soglia_LUX11';
        eovar3_221[1] = 'isteresi_LUX11';
        eovar3_221[2] = 'soglia_LUX12';
        eovar3_221[3] = 'isteresi_LUX12';
        eovar3_221[4] = 'delayOFF';
        eovar3_221[5] = '';
        eovar3_221[6] = '';
        eovar3_221[7] = '';
        eovar3_221[8] = '';
        eovar3_221[9] = '';
        //=========================================================



        my.get = function () {
            var resources = [];
            //GNUTTI 159
            resources['eovar1_159'] = eovar1_159;
            resources['eovar2_159'] = eovar2_159;
            resources['eovar3_159'] = eovar3_159;
            resources['eovar04159'] = eovar4_159;
            resources['eovar5_159'] = eovar5_159;
            resources['eovar06159'] = eovar6_159;
            resources['eovar07159'] = eovar7_159;
           
            console.log("eovars", resources );
            return resources;
        };
		my.get221 = function () {
            var resources = [];
           
            //GNUTTI 221
            resources['eovar01221'] = eovar1_221;
            resources['eovar02221'] = eovar2_221;
            resources['eovar03221'] = eovar3_221;
            console.log("eovars221", resources );
            return resources;
        };

        return my;
    })({})

})(jQuery);


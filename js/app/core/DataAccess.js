﻿(function ($) {
    $.DataAccess = (function (my) {


        //========================================================================================
        //BASE METHODS
        //========================================================================================
        my.login = function (username, password) {
            var endpoint = '/auth';
            return operation_post_login(endpoint, JSON.stringify({}), username, password);
        };
        my.changePassword = function (username, password, id, msg) {
            var endpoint = '/utenti/'+id+'/password';
            return operation_put_password(endpoint, JSON.stringify(msg), username, password);
        };


        //========================================================================================
        //BASE NODE ODIN API
        //========================================================================================

        my.ticketStat = function (modulo, params) {
            console.log("params", params);
            var endpoint = '/tickets/stats/?access_token='+localStorage.token+'&sort=-data_creazione';
            if ( params !="" && params != "none") {
                $.each(params, function (key, value) {
                    endpoint += "&" + key + '=' + value;
                })
            }
            console.log("endpoint", endpoint);
            return operation_get(endpoint, "");
        };

        my.listDuplicate = function (modulo, params, idDuplicato) {
            console.log("params", params);
            var endpoint = '/'+modulo+'/duplicates/' + idDuplicato +'?access_token='+localStorage.token+'&sort=-data_creazione';
            if ( params !="" && params != "none") {
                $.each(params, function (key, value) {
                    endpoint += "&" + key + '=' + value;
                })
            }
            console.log("endpoint", endpoint);
            return operation_get(endpoint, "");
        };
        
		my.list = function (modulo, params) {
		    console.log("params", params);
		    var endpoint = '/'+modulo +'?access_token='+localStorage.token+'&sort=-data_creazione';
            if ( params !="" && params != "none") {
                $.each(params, function (key, value) {
                    endpoint += "&" + key + '=' + value;
                })
            }
            console.log("endpoint", endpoint);
            return operation_get(endpoint, "");
        };

        my.listTicket = function (modulo, params, role, department) {
            console.log("params", params);
            if (role == "admin"){
                var endpoint = '/'+modulo +'?access_token='+localStorage.token+'&sort=-data_creazione';
                if ( params !="" && params != "none") {
                    $.each(params, function (key, value) {
                        endpoint += "&" + key + '=' + value;
                    })
                }
                console.log("endpoint", endpoint);
                return operation_get(endpoint, "");

            } else {
                var endpoint = '/'+modulo +'?access_token='+localStorage.token+'&department='+department;
                if ( params !="" && params != "none") {
                    $.each(params, function (key, value) {
                        endpoint += "&" + key + '=' + value;
                    })
                }
                console.log("endpoint", endpoint);
                return operation_get(endpoint, "");
            }

        };

        my.listFigli = function (modulo, params) {
            console.log("params", params);
            var endpoint = '/'+modulo +'/children?access_token='+localStorage.token+'&sort=-data_creazione';
            if ( params !="" && params != "none") {
                console.log("dentro");
                $.each(params, function (key, value) {
                    endpoint += "&" + key + '=' + value;
                })
            }
            console.log("endpoint", endpoint);
            return operation_get(endpoint, "");
        };
        my.read = function (modulo,id) {
            var endpoint = '/'+modulo +'/' + id +'?access_token='+localStorage.token;
            return operation_get(endpoint, JSON.stringify({}));
        };
		my.del = function (modulo, id) {
            var endpoint = '/'+modulo +'/' + id +'?access_token='+localStorage.token;
            console.log("endpoint", endpoint);
            return operation_delete(endpoint , JSON.stringify({}));
        };
		my.update = function (modulo, id, msg) {
		    console.log("LOGGGG",modulo, id, msg);
            var endpoint = '/'+modulo +'/' + id +'?access_token='+localStorage.token;
            return operation_put(endpoint, JSON.stringify(msg));
        };
        my.update_many = function (modulo, msg) {
            var endpoint = '/'+modulo+'?access_token='+localStorage.token;
            return operation_put(endpoint, JSON.stringify(msg));
        };
		my.create = function (modulo, msg) {
            var endpoint = '/'+modulo  +'?access_token='+localStorage.token;
            return operation_post(endpoint, JSON.stringify(msg));
        };

        my.files = function (modulo,id) {
            var endpoint = '/'+modulo + '?id_cliente='+ id +'&access_token='+localStorage.token;
            return operation_get(endpoint, JSON.stringify({}));
        };

        my.tipiServizi = function (modulo) {

            var endpoint = '/'+modulo + '?access_token='+localStorage.token;
            return operation_get(endpoint, JSON.stringify({}));
        };
        my.retrieveMail = function (msg) {
            var endpoint = '/password-resets?access_token=3j2Lle6prxRZ2HGFGNLU3ereY7DdskjG';
            return operation_post(endpoint, JSON.stringify(msg));
        };

        //========================================================================================
        //LABEL
        //========================================================================================



        //========================================================================================
        //METHODS
        //========================================================================================

        function noConnection(xhr, status) {
            $.toast({
                heading: 'Errore di comunicazione',
                text: xhr.responseJSON.error.message,
                position: 'top-left',
                loaderBg:'#ff6849',
                icon: 'info',
                hideAfter: 3500,

                stack: 6
            })

        }
        
        function operation_delete (endpoint, data) {
            var urlBase = $.appParms.urlBase();
            return $.ajax({
                contentType: "application/json; charset=utf-8",
                datatype: 'json',
                url: urlBase + endpoint,
                // headers: {
                //     "x-access-token": localStorage.token
                // },
                data: data,
                type: 'DELETE',
                async: true,
                beforeSend: function () {},
                complete: function () {},
                error: function (xhr, status) {
                    noConnection(xhr, status);

                }
            }); //ajax             
        }
        
        function operation_get (endpoint, data) {
            var urlBase = $.appParms.urlBase();

            return $.ajax({
                datatype: 'json',
                url: urlBase + endpoint,
                // headers: {
                //     "x-access-token": localStorage.token
                // },
                data: data,
                type: 'GET',
                async: true,
                cache: false,
                beforeSend: function () {},
                complete: function () {},
                error: function (xhr, status) {
                    noConnection(xhr, status);
                }
            }); //ajax           
        }
        function operation_get_sync(endpoint, data) {
            var urlBase = $.appParms.urlBase();
            return $.ajax({
                datatype: 'json',
                url: urlBase + endpoint,
                // headers: {
                //     "x-access-token": localStorage.token
                // },
                data: data,
                type: 'GET',
                async: false,
                beforeSend: function () { },
                complete: function () { },
                error: function (xhr, status) {
                    noConnection(xhr, status);
                }
            }); //ajax           
        }

        function operation_get_event (endpoint, data) {
            var urlBase = $.appParms.urlEvent();
            return $.ajax({
                datatype: 'json',
                url: urlBase + endpoint,
                // headers: {
                //     "x-access-token": localStorage.token
                // },
                data: data,
                type: 'GET',
                async: true,
                cache: false,
                beforeSend: function () {},
                complete: function () {},
                error: function (xhr, status) {
                    noConnection();
                }
            }); //ajax
        }
        
        function operation_post (endpoint, data) {
            var urlBase = $.appParms.urlBase();
            return $.ajax({
                contentType: "application/json; charset=utf-8",
                datatype: 'json',
                url: urlBase + endpoint,
                // headers: {
                //     "x-access-token": localStorage.token
                // },
                data: data,
                type: 'POST',
                cache: false,
                processData: false,
                async: true,
                beforeSend: function () {},
                complete: function () {},
                error: function (xhr, status) {
                    console.log("STATUS", status);
                    noConnection(xhr, status);
                }
            }); //ajax            
        }

        function operation_post_command (endpoint, data) {
            var urlBase = $.appParms._ulrCommandLayer();
            return $.ajax({
                contentType: "application/json; charset=utf-8",
                datatype: 'json',
                url: urlBase + endpoint,
                headers: {
                    "x-access-token": localStorage.token
                },
                data: data,
                type: 'POST',
                cache: false,
                processData: false,
                async: true,
                beforeSend: function () {},
                complete: function () {},
                error: function (xhr, status) {
                    noConnection();
                }
            }); //ajax
        }
        
        function operation_put (endpoint, data) {
            var urlBase = $.appParms.urlBase();
            return $.ajax({
                contentType: "application/json; charset=utf-8",
                datatype: 'json',
                url: urlBase + endpoint,
                headers: {
                    "x-access-token": localStorage.token
                },
                data: data,
                type: 'PUT',
                cache: false,
                processData: false,
                async: true,
                beforeSend: function () {},
                complete: function () {},
                error: function (xhr, status) {
                    noConnection(xhr, status);
                }
            }); //ajax           
        }

        function operation_post_login (endpoint, data,username, password) {

            function make_base_auth(user, password) {
                var tok = user + ':' + password;
                var hash = btoa(tok);
                return 'Basic ' + hash;
                
            }

            var urlBase = $.appParms.urlBase();
            return $.ajax({
                contentType: "application/json; charset=utf-8",
                datatype: 'json',
                url: urlBase + endpoint,
                // headers: {
                //     "authorization": "Authorization", "Basic" + btoa(username + ":" + password)
                // },
                data: data,
                type: 'POST',
                cache: false,
                processData: false,
                async: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', make_base_auth(username, password));
                },
                complete: function () {},
                error: function (xhr, status) {
                    noConnection(xhr, status);
                }
            }); //ajax
        }

        function operation_put_password (endpoint, data, username, password) {

            function make_base_auth(user, password) {
                var tok = user + ':' + password;
                var hash = btoa(tok);
                return 'Basic ' + hash;

            }

            var urlBase = $.appParms.urlBase();
            return $.ajax({
                contentType: "application/json; charset=utf-8",
                datatype: 'json',
                url: urlBase + endpoint,
                // headers: {
                //     "authorization": "Authorization", "Basic" + btoa(username + ":" + password)
                // },
                data: data,
                type: 'PUT',
                cache: false,
                processData: false,
                async: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', make_base_auth(username, password));
                },
                complete: function () {},
                error: function (xhr, status) {
                    noConnection();
                }
            }); //ajax
        }

        return my;        

    })({});
})(jQuery)
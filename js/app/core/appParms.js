(function ($) {

    $.appParms = (function (my) {


        // var _urlBase = "https://odin-core.herokuapp.com"; //SERVER PROD
        //
        var _urlBase = "https://latovus-dev.herokuapp.com"; //SERVER DEV

        // var _urlBase = "https://crm-sinapps-core-dev.herokuapp.com"; //SERVER DEV SINAPPS

        // var _urlEvent = "https://clv-event-layer.innowatio-aws.com/"
        // var _ulrCommandLayer = "https://clv-command-layer.innowatio-aws.com"; //command layer
        // var _ulrSso = "https://clv-sso.innowatio-aws.com"; //login e manage utenti sso


        //var _tokenTry = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjU5ZDc0NTUzOWNhMjJiMDAxMjBmZTE0OSIsImlhdCI6MTUwNzI4MzE2MX0.veJWhxlPB1PJZQ9gzhynGVvk9X-RjpRELGOUQw9rtas";
        var _tokenTry = localStorage.token;

        //var _urlRoot = 'http://localhost:3000/';
        //var _urlserver = 'http://localhost:3000/';
        //var _urlGlobal = 'http://localhost:3000/';
        //var _urlBase = "http://localhost:3000/";
        // var _urlHub = 'http://localhost:3000/signalr/hubs/';
        // var _rdp = "http://localhost:3000/rdp.ashx";

        my.urlRoot = function () {
            return _urlRoot;
        };
        my.urlEvent = function () {
            return _urlEvent;
        };
        my.urlserver = function () {
            return _urlserver;
        };
        my.urlBase = function () {
            return _urlBase;
        };
        my.ulrCommandLayer = function () {
            return _ulrCommandLayer;
        };
        my.ulrSso = function () {
            return _ulrSso;
        };
        my.tokenTry = function () {
            return _tokenTry;
        };
        my.urlGlobal = function () {
            return _urlGlobal;
        }
        my.rdp = function () {
            return _rdp;
        };
        my.key = function () {
            return _key;
        };
        my.iv = function () {
            return _iv;
        };
        my.getTime = function () {
            return new Date().getTime();
        };
        my.tokenOperator = function () {
            return _token;
        };
        return my;
    })({});

})(jQuery)

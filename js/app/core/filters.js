(function ($) {
    $.module = (function (my) {
        var $currentModule = '';
        var $errors = [];
        var $counter = 0 ;

/**************************************/
/* Date Formats*/
/*************************************/
		my.formatDateOut = function (date){
			var objDate = moment(date);
			var out = objDate.format("DD-MM-YYYY HH:mm");
			if (!objDate._isValid) out = "";
			return out;
		};
/**************************************/
/* Set Picker  OBBLIGATORI*/
/*************************************/
        my.setpicker = function (){
            var checkin1 = $('.start').datetimepicker({

                format: 'dd-mm-yyyy hh:ii',
                todayBtn: true,

                autoclose: true

            }).on('changeDate', function (ev) {
                console.log("ev", ev)


                var newDate = new Date(ev.date);
                //newDate.setDate(newDate.getDate() + 1 );  mette un giorno in piu del piker 1
                newDate.setDate(newDate.getDate() );   //mette la stessa data del picker 1
				// newDate.setHours(newDate.getHours() - 1);
                console.log("newDate", newDate);
                checkout1.datetimepicker("update", newDate);


                $('.end')[0].focus();
            });


            var checkout1 = $('.end').datetimepicker({
                beforeShowDay: function (date) {
                    console.log("date", date);
                    if (!checkin1.datetimepicker("getDate").valueOf()) {
                        return date.valueOf() >= new Date().valueOf();
                    } else {
                        return date.valueOf() > checkin1.datetimepicker("getDate").valueOf();
                    }
                },
                format: 'dd-mm-yyyy hh:ii',
                todayBtn: true,

                autoclose: true

            }).on('changeDate', function (ev) {});

        }

        my.setpickerGeneral = function (){


            jQuery('.generalPicker').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy',
                forceParse: false
            });


            $('.generalTimePicker').datetimepicker({

                format: 'dd-mm-yyyy hh:ii',
                todayBtn: true,
                autoclose: true,
                forceParse: false

            })

        }

/**************************************/
/* CHECK input validity*/
/*************************************/
	function checkInputType(name, type, value){
			// console.log ("Checkin input", name, type, value);
			switch(type) {
				case "email":
					var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
					if (!re.test(value)) {$counter++;$errors.push(name + " non è nel formato giusto")};
					break;
				case "equal11":
					if ((value.toString()).length!=11) {$counter++;$errors.push( name + " deve essere di 11 caratteri");}
					break;
				case "equal16":
					if ((value.toString()).length!=16) {$counter++;$errors.push( name + " deve essere di 16 caratteri");}
					break;
				case "min6":
					if ((value.toString()).length<=6) {$counter++;$errors.push( name + " deve essere di almeno 6 caratteri");}
					break;
				case "dateConsistency":
					// console.log (moment(value, "DD-MM-YYYY HH:mm").local().toDate(), moment($("#end").val(), "DD-MM-YYYY HH:mm").local().toDate(), "consistency2");
					if (localStorage.module == "eventi" || localStorage.inpage == "create" )
					{
						if (moment(value, "DD-MM-YYYY HH:mm").local().toDate()>= moment($("#endAdd").val(), "DD-MM-YYYY HH:mm").local().toDate()) {
							$counter++;
							$errors.push( "la data di fine deve essere posteriore a quella di inizio");
						}
					}	
					else{
						
						if (moment(value, "DD-MM-YYYY HH:mm").local().toDate()>= moment($("#end").val(), "DD-MM-YYYY HH:mm").local().toDate()) {$counter++;$errors.push( "la data di fine deve essere posteriore a quella di inizio");}
					}
					break;
				
				
			}	
		}

/**************************************/
/* OTHERS*/
/*************************************/




        return my;
    })({});
	
    })(jQuery);